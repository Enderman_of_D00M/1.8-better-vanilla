package net.minecraft.entity.ai;

import java.util.Iterator;
import java.util.List;

import net.minecraft.entity.monster.EntityGolem;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.util.Vec3;

public class EntityAIGuardVillager extends EntityAIBase
{
    EntityGolem guardingMob;
    EntityVillager villager;
    double movementSpeed;
    private int field_75345_d;
    private static final String __OBFID = "CL_00001586";

    public EntityAIGuardVillager(EntityGolem p_i1626_1_, double p_i1626_2_)
    {
        this.guardingMob = p_i1626_1_;
        this.movementSpeed = p_i1626_2_;
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute()
    {
        if (this.guardingMob.getAttackTarget() == null)
        {
            return false;
        }
        else
        {
            List list = this.guardingMob.worldObj.getEntitiesWithinAABB(EntityVillager.class, this.guardingMob.getEntityBoundingBox().expand(16.0D, 16.0D, 16.0D));
            
            if (list.isEmpty())
            {
                return false;
            }
            else
            {
                Iterator iterator = list.iterator();

                while (iterator.hasNext())
                {
                	EntityVillager entityanimal1 = (EntityVillager)iterator.next();

                    this.villager = entityanimal1;
                }
                
                return this.villager != null;
            }
        }
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean continueExecuting()
    {
        if (this.guardingMob.getAttackTarget() == null)
        {
            return false;
        }
        else if (!this.villager.isEntityAlive())
        {
            return false;
        }
        else
        {
            double d0 = this.guardingMob.getDistanceSqToEntity(this.villager);
            return d0 >= 16.0D && d0 <= 256.0D;
        }
    }

    /**
     * Execute a one shot task or start executing a continuous task
     */
    public void startExecuting()
    {
        this.field_75345_d = 0;
        this.guardingMob.getNavigator().tryMoveToXYZ(this.villager.posX + (double)(this.guardingMob.rand.nextFloat() * 10.0F - 5.0F), this.villager.posY, this.villager.posZ + (double)(this.guardingMob.rand.nextFloat() * 10.0F - 5.0F), this.movementSpeed);
    }

    /**
     * Resets the task
     */
    public void resetTask()
    {
        this.villager = null;
    }

    /**
     * Updates the task
     */
    public void updateTask()
    {
        if (this.guardingMob.getRNG().nextInt(120) == 0 && this.guardingMob.getAttackTarget() == null)
        {
            this.guardingMob.getNavigator().tryMoveToXYZ(this.villager.posX + (double)(this.guardingMob.rand.nextFloat() * 10.0F - 5.0F), this.villager.posY, this.villager.posZ + (double)(this.guardingMob.rand.nextFloat() * 10.0F - 5.0F), this.movementSpeed);
        }
    }
}