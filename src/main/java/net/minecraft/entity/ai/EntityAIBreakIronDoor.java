package net.minecraft.entity.ai;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.material.Material;
import net.minecraft.entity.EntityLiving;
import net.minecraft.util.BlockPos;
import net.minecraft.world.EnumDifficulty;

public class EntityAIBreakIronDoor extends EntityAIDoorInteract
{
    private int breakingTime;
    private int previousBreakProgress = -1;
    private static final String __OBFID = "CL_00001577";

    public EntityAIBreakIronDoor(EntityLiving entityIn)
    {
        super(entityIn);
    }
    
    public BlockDoor func_179506_a(BlockPos p_179506_1_)
    {
        Block block = this.theEntity.worldObj.getBlockState(p_179506_1_).getBlock();
        return block instanceof BlockDoor && block.getMaterial() == Material.iron ? (BlockDoor)block : null;
    }

    /**
     * Returns whether the EntityAIBase should begin execution.
     */
    public boolean shouldExecute()
    {
        if (!super.shouldExecute())
        {
            return false;
        }
        else if (!this.theEntity.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"))
        {
            return false;
        }
        else
        {
            BlockDoor blockdoor = this.doorBlock;
            return !BlockDoor.isOpen(this.theEntity.worldObj, this.doorPosition);
        }
    }

    /**
     * Execute a one shot task or start executing a continuous task
     */
    public void startExecuting()
    {
        super.startExecuting();
        this.breakingTime = 0;
    }

    /**
     * Returns whether an in-progress EntityAIBase should continue executing
     */
    public boolean continueExecuting()
    {
        double d0 = this.theEntity.getDistanceSq(this.doorPosition);
        boolean flag;

        if (this.breakingTime <= 400)
        {
            BlockDoor blockdoor = this.doorBlock;

            if (!BlockDoor.isOpen(this.theEntity.worldObj, this.doorPosition) && d0 < 4.0D)
            {
                flag = true;
                return flag;
            }
        }

        flag = false;
        return flag;
    }

    /**
     * Resets the task
     */
    public void resetTask()
    {
        super.resetTask();
        this.theEntity.worldObj.sendBlockBreakProgress(this.theEntity.getEntityId(), this.doorPosition, -1);
    }

    /**
     * Updates the task
     */
    public void updateTask()
    {
        super.updateTask();

        if (this.theEntity.getRNG().nextInt(20) == 0)
        {
            this.theEntity.playSound("mob.zombie.metal", 1.0F, 1.0F);
        }

        ++this.breakingTime;
        int i = (int)((float)this.breakingTime / 400.0F * 10.0F);

        if (i != this.previousBreakProgress)
        {
            this.theEntity.worldObj.sendBlockBreakProgress(this.theEntity.getEntityId(), this.doorPosition, i);
            this.previousBreakProgress = i;
        }

        if (this.breakingTime == 400 && this.theEntity.worldObj.getDifficulty() == EnumDifficulty.HARD)
        {
            this.theEntity.worldObj.setBlockToAir(this.doorPosition);
            this.theEntity.playSound("mob.zombie.woodbreak", 1.0F, 0.5F);
            this.theEntity.worldObj.playAuxSFX(2001, this.doorPosition, Block.getIdFromBlock(this.doorBlock));
        }
    }
}
