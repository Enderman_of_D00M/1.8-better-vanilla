package net.minecraft.entity.boss;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import java.util.Iterator;
import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.command.IEntitySelector;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIArrowAttack;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityBlaze;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.monster.EntityPigZombie;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntityWitherMinion;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.entity.projectile.EntityWitherSkull;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.stats.AchievementList;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityWitherGood extends EntityMob implements IBossDisplayData, IRangedAttackMob
{
    private float[] field_82220_d = new float[2];
    private float[] field_82221_e = new float[2];
    private float[] field_82217_f = new float[2];
    private float[] field_82218_g = new float[2];
    private int[] field_82223_h = new int[2];
    private int[] field_82224_i = new int[2];
    /** Time before the Wither tries to break blocks */
    private int blockBreakCounter;
    /** Selector used to determine the entities a wither boss should attack. */
    private static final Predicate attackEntitySelector = new Predicate()
    {
        private static final String __OBFID = "CL_00001662";
        
        public boolean func_180027_a(Entity p_180027_1_)
        {
            return p_180027_1_ instanceof EntityLivingBase && !p_180027_1_.isInAlliedSystem || p_180027_1_.isCorrupted;
        }
        public boolean apply(Object p_apply_1_)
        {
            return this.func_180027_a((Entity)p_apply_1_);
        }
    };
    private static final String __OBFID = "CL_00001661";

    public EntityWitherGood(World worldIn)
    {
        super(worldIn);
        
        int b0 = 40;

        if (this.dangerLevel1())
        {
            b0 = 20;
        }
        
        double d = this.getEntityAttribute(SharedMonsterAttributes.followRange).getAttributeValue();
        
        this.isInAlliedSystem = true;
        this.isCorrupted = false;
        this.setHealth(this.getMaxHealth());
        this.setSize(0.9F, 3.5F);
        this.isImmuneToFire = true;
        this.isImmuneToWither = true;
        ((PathNavigateGround)this.getNavigator()).func_179693_d(true);
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(2, new EntityAIArrowAttack(this, 1.0D, b0, (float)d));
        this.tasks.addTask(5, new EntityAIWander(this, 0.4D));
        this.tasks.addTask(6, new EntityAIWatchClosest(this, EntityLivingBase.class, 8.0F));
        this.tasks.addTask(7, new EntityAILookIdle(this));
        this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, false, new Class[0]));
        this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityLivingBase.class, 0, false, false, attackEntitySelector));
        this.experienceValue = 5000;
    }

    protected void entityInit()
    {
        super.entityInit();
        this.dataWatcher.addObject(17, new Integer(0));
        this.dataWatcher.addObject(18, new Integer(0));
        this.dataWatcher.addObject(19, new Integer(0));
        this.dataWatcher.addObject(20, new Integer(0));
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound tagCompound)
    {
        super.writeEntityToNBT(tagCompound);
        tagCompound.setInteger("Invul", this.getInvulTime());
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound tagCompund)
    {
        super.readEntityFromNBT(tagCompund);
        this.setInvulTime(tagCompund.getInteger("Invul"));
    }

    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound()
    {
        return "mob.wither.idle";
    }

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "mob.wither.hurt";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "mob.wither.death";
    }

    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    public void onLivingUpdate()
    {
        if (this.rand.nextInt(100) == 0 && this.getHealth() > 0 && !this.worldObj.isRemote)
        {
        	this.addPotionEffect(new PotionEffect(Potion.regeneration.id, 100, 0));
        }
    	
        int b0 = 0;

        if (this.dangerLevel1())
        {
            b0 = 120;
        }
        else if (this.dangerLevel2())
        {
            b0 = 40;
        }
    	
        if (b0 > 0 && this.rand.nextInt(b0) == 0 && this.getHealth() > 0 && !this.worldObj.isRemote)
        {
            switch (this.rand.nextInt(5))
            {
                case 0:
                    EntityZombie entitychicken = new EntityZombie(this.worldObj);
                    entitychicken.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0F);
                    entitychicken.func_180482_a(this.worldObj.getDifficultyForLocation(new BlockPos(entitychicken)), (IEntityLivingData)null);
                    this.worldObj.spawnEntityInWorld(entitychicken);
                    break;
                case 1:
                    EntitySkeleton entitychicken1 = new EntitySkeleton(this.worldObj);
                    entitychicken1.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0F);
                    entitychicken1.func_180482_a(this.worldObj.getDifficultyForLocation(new BlockPos(entitychicken1)), (IEntityLivingData)null);
                    this.worldObj.spawnEntityInWorld(entitychicken1);
                    break;
                case 2:
                    EntitySkeleton entitychicken11 = new EntitySkeleton(this.worldObj);
                    entitychicken11.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0F);
                    entitychicken11.func_180482_a(this.worldObj.getDifficultyForLocation(new BlockPos(entitychicken11)), (IEntityLivingData)null);
                	entitychicken11.setSkeletonType(1);
                	entitychicken11.setCurrentItemOrArmor(0, new ItemStack(Items.stone_sword));
                    this.worldObj.spawnEntityInWorld(entitychicken11);
                    break;
                case 3:
                    EntityPigZombie entitychicken111 = new EntityPigZombie(this.worldObj);
                    entitychicken111.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0F);
                    entitychicken111.func_180482_a(this.worldObj.getDifficultyForLocation(new BlockPos(entitychicken111)), (IEntityLivingData)null);
                    this.worldObj.spawnEntityInWorld(entitychicken111);
                    break;
                case 4:
                    EntityBlaze entitychicken1111 = new EntityBlaze(this.worldObj);
                    entitychicken1111.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0F);
                    entitychicken1111.func_180482_a(this.worldObj.getDifficultyForLocation(new BlockPos(entitychicken1111)), (IEntityLivingData)null);
                    this.worldObj.spawnEntityInWorld(entitychicken1111);
            }
        }
    	
        if (!this.onGround && this.motionY < 0.0D)
        {
            this.motionY *= 0.6D;
        }
        
        double d1;
        double d3;
        double d5;

        double speed = this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).getAttributeValue();
        
        if (!this.worldObj.isRemote && this.getWatchedTargetId(0) > 0)
        {
            Entity entity = this.worldObj.getEntityByID(this.getWatchedTargetId(0));

            if (entity != null)
            {
            	if (!(entity instanceof EntityWither))
            	{
                    if (this.posY < entity.posY || !this.isArmored() && this.posY < entity.posY + 6.0D + entity.getEyeHeight())
                    {
                        if (this.motionY < 0.0D)
                        {
                            this.motionY = 0.0D;
                        }

                        this.motionY += (speed - this.motionY) * speed;
                    }
            	}

                double d0 = entity.posX - this.posX;
                d1 = entity.posZ - this.posZ;
                d3 = d0 * d0 + d1 * d1;

                if (d3 > 1.0D)
                {
                    d5 = (double)MathHelper.sqrt_double(d3);
                    this.motionX += (d0 / d5 * speed - this.motionX) * speed;
                    this.motionZ += (d1 / d5 * speed - this.motionZ) * speed;
                }
            }
        }

        if (this.motionX * this.motionX + this.motionZ * this.motionZ > 0.05000000074505806D)
        {
            this.rotationYaw = (float)Math.atan2(this.motionZ, this.motionX) * (180F / (float)Math.PI) - 90.0F;
        }

        super.onLivingUpdate();
        int i;

        for (i = 0; i < 2; ++i)
        {
            this.field_82218_g[i] = this.field_82221_e[i];
            this.field_82217_f[i] = this.field_82220_d[i];
        }

        int j;

        for (i = 0; i < 2; ++i)
        {
            j = this.getWatchedTargetId(i + 1);
            Entity entity1 = null;

            if (j > 0)
            {
                entity1 = this.worldObj.getEntityByID(j);
            }

            if (entity1 != null)
            {
                d1 = this.func_82214_u(i + 1);
                d3 = this.func_82208_v(i + 1);
                d5 = this.func_82213_w(i + 1);
                double d6 = entity1.posX - d1;
                double d7 = entity1.posY + (double)entity1.getEyeHeight() - d3;
                double d8 = entity1.posZ - d5;
                double d9 = (double)MathHelper.sqrt_double(d6 * d6 + d8 * d8);
                float f = (float)(Math.atan2(d8, d6) * 180.0D / Math.PI) - 90.0F;
                float f1 = (float)(-(Math.atan2(d7, d9) * 180.0D / Math.PI));
                this.field_82220_d[i] = this.func_82204_b(this.field_82220_d[i], f1, 40.0F);
                this.field_82221_e[i] = this.func_82204_b(this.field_82221_e[i], f, 10.0F);
            }
            else
            {
                this.field_82221_e[i] = this.func_82204_b(this.field_82221_e[i], this.renderYawOffset, 10.0F);
            }
        }

        boolean flag = this.isArmored();

        for (j = 0; j < 3; ++j)
        {
            double d10 = this.func_82214_u(j);
            double d2 = this.func_82208_v(j);
            double d4 = this.func_82213_w(j);
            this.worldObj.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, d10 + this.rand.nextGaussian() * 0.30000001192092896D, d2 + this.rand.nextGaussian() * 0.30000001192092896D, d4 + this.rand.nextGaussian() * 0.30000001192092896D, 0.0D, 0.0D, 0.0D, new int[0]);

            if (flag && this.worldObj.rand.nextInt(4) == 0)
            {
                this.worldObj.spawnParticle(EnumParticleTypes.SPELL_MOB, d10 + this.rand.nextGaussian() * 0.30000001192092896D, d2 + this.rand.nextGaussian() * 0.30000001192092896D, d4 + this.rand.nextGaussian() * 0.30000001192092896D, 0.699999988079071D, 0.699999988079071D, 0.5D, new int[0]);
            }
        }
        
        for (int i1 = 0; i1 < 2; ++i1)
        {
            this.worldObj.spawnParticle(EnumParticleTypes.FIREWORKS_SPARK, this.posX + (this.rand.nextDouble() - 0.5D) * (double)this.width, this.posY + this.rand.nextDouble() * (double)this.height, this.posZ + (this.rand.nextDouble() - 0.5D) * (double)this.width, 0.0D, 0.0D, 0.0D, new int[0]);
        }

        if (this.getInvulTime() > 0)
        {
            for (j = 0; j < 3; ++j)
            {
                this.worldObj.spawnParticle(EnumParticleTypes.SPELL_MOB, this.posX + this.rand.nextGaussian() * 1.0D, this.posY + (double)(this.rand.nextFloat() * 3.3F), this.posZ + this.rand.nextGaussian() * 1.0D, 0.699999988079071D, 0.699999988079071D, 0.8999999761581421D, new int[0]);
            }
        }
    }

    protected void updateAITasks()
    {
        List list1 = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(16.0D, 16.0D, 16.0D));
        
        if (list1 != null && !list1.isEmpty())
        {
            for (int i = 0; i < list1.size(); ++i)
            {
                Entity entity = (Entity)list1.get(i);

                if (entity instanceof EntityLivingBase && !(entity instanceof EntityWitherGood) && entity.isInAlliedSystem && ((EntityLivingBase) entity).getCreatureAttribute() == EnumCreatureAttribute.UNDEAD)
                {
                    byte b0 = -1;

                    if (this.worldObj.getDifficulty() == EnumDifficulty.EASY)
                    {
                        b0 = 0;
                    }
                    else if (this.worldObj.getDifficulty() == EnumDifficulty.NORMAL)
                    {
                        b0 = 1;
                    }
                    else if (this.worldObj.getDifficulty() == EnumDifficulty.HARD)
                    {
                        b0 = 2;
                    }

                    if (b0 > -1)
                    {
                    	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.damageBoost.id, 200, b0));
                    	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.fireResistance.id, 200, 0));
                    	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.moveSpeed.id, 200, b0));
                    	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.resistance.id, 200, b0));
                    	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.waterBreathing.id, 200, 0));
                    }
                }
            }
        }
        
        List list11 = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(16.0D, 16.0D, 16.0D));
        
        if (list11 != null && !list11.isEmpty())
        {
            for (int i = 0; i < list11.size(); ++i)
            {
                Entity entity = (Entity)list11.get(i);

                byte b0 = 0;

                if (this.worldObj.getDifficulty() == EnumDifficulty.EASY)
                {
                    b0 = 100;
                }
                else if (this.worldObj.getDifficulty() == EnumDifficulty.NORMAL)
                {
                    b0 = 60;
                }
                else if (this.worldObj.getDifficulty() == EnumDifficulty.HARD)
                {
                    b0 = 40;
                }

                if (b0 > 0)
                {
                    if (entity instanceof EntityLivingBase && !(entity instanceof EntityWitherGood) && this.ticksExisted % b0 == 0 && ((EntityLivingBase) entity).getCreatureAttribute() != EnumCreatureAttribute.UNDEAD)
                    {
                    	if (entity.isInAlliedSystem)
                    	{
                        	((EntityLivingBase) entity).heal(1.0F);
                    	}
                    	else
                    	{
                        	entity.attackEntityFrom(DamageSource.wither, 1.0F);
                        	this.heal(1.0F);
                    	}
                    }
                }
            }
        }
    	
        int i;

        if (this.getInvulTime() > 0)
        {
            i = this.getInvulTime() - 1;

            if (i <= 0)
            {
                this.worldObj.newExplosion(this, this.posX, this.posY + (double)this.getEyeHeight(), this.posZ, 7.0F, false, this.worldObj.getGameRules().getGameRuleBooleanValue("explosionDamage"));
                this.worldObj.playBroadcastSound(1013, new BlockPos(this), 0);
            }

            this.setInvulTime(i);

            if (this.ticksExisted % 10 == 0)
            {
                this.heal(100.0F);
            }
        }
        else
        {
            super.updateAITasks();
            int i1;

            double d = this.getEntityAttribute(SharedMonsterAttributes.followRange).getAttributeValue();
            
            for (i = 1; i < 3; ++i)
            {
                EntityPlayer aggro = this.worldObj.getClosestPlayerToEntity(this, d);
                
                if (aggro != null && !aggro.capabilities.disableDamage)
                {
                    this.func_82211_c(i, aggro.getEntityId());
                }
            	
                if (this.ticksExisted >= this.field_82223_h[i - 1])
                {
                    this.field_82223_h[i - 1] = this.ticksExisted + 10 + this.rand.nextInt(10);

                    if (this.worldObj.getDifficulty() == EnumDifficulty.NORMAL || this.worldObj.getDifficulty() == EnumDifficulty.HARD)
                    {
                        int k2 = i - 1;
                        int l2 = this.field_82224_i[i - 1];
                        this.field_82224_i[k2] = this.field_82224_i[i - 1] + 1;

                        if (l2 > 15)
                        {
                            float f = 10.0F;
                            float f1 = 5.0F;
                            double d0 = MathHelper.getRandomDoubleInRange(this.rand, this.posX - (double)f, this.posX + (double)f);
                            double d1 = MathHelper.getRandomDoubleInRange(this.rand, this.posY - (double)f1, this.posY + (double)f1);
                            double d2 = MathHelper.getRandomDoubleInRange(this.rand, this.posZ - (double)f, this.posZ + (double)f);
                            EntityPlayer entityplayer = this.worldObj.getClosestPlayerToEntity(this, 16.0D);
                            if (entityplayer != null)
                            {
                                d0 = entityplayer.posX;
                                d1 = entityplayer.posY + entityplayer.getEyeHeight();
                                d2 = entityplayer.posZ;
                            }
                            this.launchWitherSkullToCoords(i + 1, d0, d1, d2, true);
                            this.field_82224_i[i - 1] = 0;
                        }
                    }

                    i1 = this.getWatchedTargetId(i);
                    
                    int b0 = 40;

                    if (this.dangerLevel1())
                    {
                        b0 = 20;
                    }

                    if (i1 > 0)
                    {
                        Entity entity = this.worldObj.getEntityByID(i1);

                        if (entity != null && entity.isEntityAlive() && this.getDistanceSqToEntity(entity) <= d * d)
                        {
                            this.launchWitherSkullToEntity(i + 1, (EntityLivingBase)entity);
                            this.field_82223_h[i - 1] = this.ticksExisted + b0 + this.rand.nextInt(b0 / 2);
                            this.field_82224_i[i - 1] = 0;
                        }
                        else
                        {
                            this.func_82211_c(i, 0);
                        }
                    }
                    else
                    {
                        List list = this.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, this.getEntityBoundingBox().expand(d, d, d), Predicates.and(attackEntitySelector, IEntitySelector.NOT_SPECTATING));

                        for (int k1 = 0; k1 < (int)d && !list.isEmpty(); ++k1)
                        {
                            EntityLivingBase entitylivingbase = (EntityLivingBase)list.get(this.rand.nextInt(list.size()));

                            if (entitylivingbase != null && entitylivingbase != this && entitylivingbase.isEntityAlive())
                            {
                                if (entitylivingbase instanceof EntityPlayer)
                                {
                                    if (!((EntityPlayer)entitylivingbase).capabilities.disableDamage)
                                    {
                                        this.func_82211_c(i, entitylivingbase.getEntityId());
                                    }
                                }
                                else
                                {
                                    this.func_82211_c(i, entitylivingbase.getEntityId());
                                }
                            }
                            else
                            {
                                list.remove(entitylivingbase);
                            }
                        }
                    }
                }
            }

            if (this.getAttackTarget() != null)
            {
                this.func_82211_c(0, this.getAttackTarget().getEntityId());
            }
            else
            {
                this.func_82211_c(0, 0);
            }

            if (this.blockBreakCounter > 0)
            {
                --this.blockBreakCounter;

                if (this.blockBreakCounter == 0 && this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"))
                {
                    i = MathHelper.floor_double(this.posY);
                    i1 = MathHelper.floor_double(this.posX);
                    int j1 = MathHelper.floor_double(this.posZ);
                    boolean flag = false;

                    for (int l1 = -1; l1 <= 1; ++l1)
                    {
                        for (int i2 = -1; i2 <= 1; ++i2)
                        {
                            for (int j = 0; j <= 3; ++j)
                            {
                                int j2 = i1 + l1;
                                int k = i + j;
                                int l = j1 + i2;
                                Block block = this.worldObj.getBlockState(new BlockPos(j2, k, l)).getBlock();

                                if (!block.isAir(worldObj, new BlockPos(j2, k, l)) && this.canBlockBeDestroyed(block, worldObj, new BlockPos(j2, k, l)))
                                {
                                    flag = this.worldObj.destroyBlock(new BlockPos(j2, k, l), true) || flag;
                                }
                            }
                        }
                    }

                    if (flag)
                    {
                        this.worldObj.playAuxSFXAtEntity((EntityPlayer)null, 1012, new BlockPos(this), 0);
                    }
                }
            }

            int b0 = 20;
            float h = 1.0F;

            if (this.dangerLevel2())
            {
                b0 = 10;
                h = 2.0F;
            }
            else if (this.dangerLevel1())
            {
                b0 = 15;
                h = 1.0F;
            }
            
            if (b0 > 0 && this.ticksExisted % b0 == 0)
            {
                this.heal(h);
            }
        }
    }
    
    public boolean canBlockBeDestroyed(Block block, IBlockAccess world, BlockPos pos)
    {
        return block != net.minecraft.init.Blocks.barrier && block != net.minecraft.init.Blocks.bedrock && block != net.minecraft.init.Blocks.end_portal && block != net.minecraft.init.Blocks.end_portal_frame && block != net.minecraft.init.Blocks.command_block;
    }

    public void func_82206_m()
    {
        this.setInvulTime(220);
        this.setHealth(this.getMaxHealth() / 3.0F);
    }

    /**
     * Sets the Entity inside a web block.
     */
    public void setInWeb() {}

    /**
     * Returns the current armor value as determined by a call to InventoryPlayer.getTotalArmorValue
     */
    public int getTotalArmorValue()
    {
        int b0 = 4;

        if (this.dangerLevel2())
        {
            b0 = 20;
        }
        else if (this.dangerLevel1())
        {
            b0 = 6;
        }
    	
        return b0;
    }

    private double func_82214_u(int p_82214_1_)
    {
        if (p_82214_1_ <= 0)
        {
            return this.posX;
        }
        else
        {
            float f = (this.renderYawOffset + (float)(180 * (p_82214_1_ - 1))) / 180.0F * (float)Math.PI;
            float f1 = MathHelper.cos(f);
            return this.posX + (double)f1 * 1.3D;
        }
    }

    private double func_82208_v(int p_82208_1_)
    {
        return p_82208_1_ <= 0 ? this.posY + 3.0D : this.posY + 2.2D;
    }

    private double func_82213_w(int p_82213_1_)
    {
        if (p_82213_1_ <= 0)
        {
            return this.posZ;
        }
        else
        {
            float f = (this.renderYawOffset + (float)(180 * (p_82213_1_ - 1))) / 180.0F * (float)Math.PI;
            float f1 = MathHelper.sin(f);
            return this.posZ + (double)f1 * 1.3D;
        }
    }

    private float func_82204_b(float p_82204_1_, float p_82204_2_, float p_82204_3_)
    {
        float f3 = MathHelper.wrapAngleTo180_float(p_82204_2_ - p_82204_1_);

        if (f3 > p_82204_3_)
        {
            f3 = p_82204_3_;
        }

        if (f3 < -p_82204_3_)
        {
            f3 = -p_82204_3_;
        }

        return p_82204_1_ + f3;
    }

    private void launchWitherSkullToEntity(int p_82216_1_, EntityLivingBase p_82216_2_)
    {
        this.launchWitherSkullToCoords(p_82216_1_, p_82216_2_.posX, p_82216_2_.posY + (double)p_82216_2_.getEyeHeight() * 0.5D, p_82216_2_.posZ, p_82216_1_ == 0 && this.rand.nextFloat() < 0.05F * this.worldObj.getDifficulty().getDifficultyId());
    }

    /**
     * Launches a Wither skull toward (par2, par4, par6)
     */
    private void launchWitherSkullToCoords(int p_82209_1_, double p_82209_2_, double p_82209_4_, double p_82209_6_, boolean p_82209_8_)
    {
        this.worldObj.playAuxSFXAtEntity((EntityPlayer)null, 1014, new BlockPos(this), 0);
        double d3 = this.func_82214_u(p_82209_1_);
        double d4 = this.func_82208_v(p_82209_1_);
        double d5 = this.func_82213_w(p_82209_1_);
        double d6 = p_82209_2_ - d3;
        double d7 = p_82209_4_ - d4;
        double d8 = p_82209_6_ - d5;
        EntityWitherSkull entitywitherskull = new EntityWitherSkull(this.worldObj, this, d6, d7, d8);

        int s = (int)this.getEntityAttribute(SharedMonsterAttributes.attackDamage).getAttributeValue();
        
        if (p_82209_8_ || this.worldObj.getDifficulty() == EnumDifficulty.HARD)
        {
            entitywitherskull.setInvulnerable(true);
        }
        
        if (p_82209_8_)
        {
            entitywitherskull.paladinStrike = true;;
        }
        
    	if (entitywitherskull.paladinStrike)
    	{
    		entitywitherskull.ajustableDamage = s * 4;
    		entitywitherskull.explosionPower = 6;
    	}
        
        if (this.dangerLevel2())
        {
        	entitywitherskull.explosionPower = 3;
        	entitywitherskull.ajustableDamage = s * 4;
            entitywitherskull.setInvulnerable(true);
        }
        else
        {
        	entitywitherskull.explosionPower = 1;
        	entitywitherskull.ajustableDamage = s;
        }

        entitywitherskull.posY = d4;
        entitywitherskull.posX = d3;
        entitywitherskull.posZ = d5;
        this.worldObj.spawnEntityInWorld(entitywitherskull);
    }

    /**
     * Attack the specified entity using a ranged attack.
     */
    public void attackEntityWithRangedAttack(EntityLivingBase p_82196_1_, float p_82196_2_)
    {
        this.launchWitherSkullToEntity(0, p_82196_1_);
    }

    /**
     * Called when the entity is attacked.
     */
    public boolean attackEntityFrom(DamageSource source, float amount)
    {
        if (this.isEntityInvulnerable(source))
        {
            return false;
        }
        else if (source != DamageSource.drown && !source.isExplosion() && !(source.getEntity() instanceof EntityWitherGood))
        {
            if (this.getInvulTime() > 0 && source != DamageSource.outOfWorld)
            {
                return false;
            }
            else
            {
                Entity entity;

                if (this.isArmored())
                {
                    entity = source.getSourceOfDamage();

                    if (entity instanceof EntityArrow)
                    {
                        return false;
                    }
                }
                
                Entity entity0;

                entity0 = source.getEntity();

                if (entity0 != null && entity0.isInAlliedSystem && this.isInAlliedSystem && !this.isCorrupted && !entity0.isCorrupted)
                {
                    return false;
                }
                else
                {
                    if (this.blockBreakCounter <= 0)
                    {
                        this.blockBreakCounter = 20;
                    }

                    for (int i = 0; i < this.field_82224_i.length; ++i)
                    {
                        this.field_82224_i[i] += 3;
                    }

                    return super.attackEntityFrom(source, amount);
                }
            }
        }
        else
        {
            return false;
        }
    }

    /**
     * Drop 0-2 items of this living's type
     */
    protected void dropFewItems(boolean p_70628_1_, int p_70628_2_)
    {
        EntityItem entityitem = this.dropItem(Items.nether_star, 1);

        if (entityitem != null)
        {
            entityitem.setNoDespawn();
        }

        if (!this.worldObj.isRemote)
        {
            Iterator iterator = this.worldObj.getEntitiesWithinAABB(EntityPlayer.class, this.getEntityBoundingBox().expand(50.0D, 100.0D, 50.0D)).iterator();

            while (iterator.hasNext())
            {
                EntityPlayer entityplayer = (EntityPlayer)iterator.next();
                entityplayer.triggerAchievement(AchievementList.killWither);
            }
        }
    }

    /**
     * Makes the entity despawn if requirements are reached
     */
    protected void despawnEntity()
    {
        this.entityAge = 0;
    }

    @SideOnly(Side.CLIENT)
    public int getBrightnessForRender(float p_70070_1_)
    {
        return 15728880;
    }

    /**
     * Gets how bright this entity is.
     */
    public float getBrightness(float p_70013_1_)
    {
        return 1.0F;
    }

    public void fall(float distance, float damageMultiplier) {}

    public boolean isPotionApplicable(PotionEffect p_70687_1_)
    {
        return p_70687_1_.getPotionID() == Potion.invisibility.id || p_70687_1_.getPotionID() == Potion.wither.id  || p_70687_1_.getPotionID() == Potion.moveSlowdown.id || p_70687_1_.getPotionID() == Potion.poison.id ? false : super.isPotionApplicable(p_70687_1_);
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(3000.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.5D);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(64.0D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(12.0D);
        this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(1.0D);
    }

    @SideOnly(Side.CLIENT)
    public float func_82207_a(int p_82207_1_)
    {
        return this.field_82221_e[p_82207_1_];
    }

    @SideOnly(Side.CLIENT)
    public float func_82210_r(int p_82210_1_)
    {
        return this.field_82220_d[p_82210_1_];
    }

    public int getInvulTime()
    {
        return this.dataWatcher.getWatchableObjectInt(20);
    }

    public void setInvulTime(int p_82215_1_)
    {
        this.dataWatcher.updateObject(20, Integer.valueOf(p_82215_1_));
    }

    /**
     * Returns the target entity ID if present, or -1 if not @param par1 The target offset, should be from 0-2
     */
    public int getWatchedTargetId(int p_82203_1_)
    {
        return this.dataWatcher.getWatchableObjectInt(17 + p_82203_1_);
    }

    public void func_82211_c(int p_82211_1_, int p_82211_2_)
    {
        this.dataWatcher.updateObject(17 + p_82211_1_, Integer.valueOf(p_82211_2_));
    }

    /**
     * Returns whether the wither is armored with its boss armor or not by checking whether its health is below half of
     * its maximum.
     */
    public boolean isArmored()
    {
        return this.getHealth() <= this.getMaxHealth() / 2.0F;
    }
    
    public boolean dangerLevel1()
    {
        return this.getHealth() <= 2400.0F;
    }
    
    public boolean dangerLevel2()
    {
        return this.getHealth() <= this.getMaxHealth() / 2.0F;
    }

    /**
     * Get this Entity's EnumCreatureAttribute
     */
    public EnumCreatureAttribute getCreatureAttribute()
    {
        return EnumCreatureAttribute.UNDEAD;
    }

    public boolean canAttackClass(Class p_70686_1_)
    {
        return p_70686_1_ != EntityWitherGood.class;
    }
}
