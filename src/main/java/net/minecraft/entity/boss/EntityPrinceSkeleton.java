package net.minecraft.entity.boss;

import java.util.List;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIArrowAttack;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.World;

public class EntityPrinceSkeleton  extends EntitySkeleton implements IBossDisplayData

{
    public int callSkeletonsRechargeTimer;
    public int armorPiercingArrowRechargeTimer;
    public int grantFireBowsTimer;
    public int chargeTimer;
	private boolean shouldtaunt;
    int shootingSpeedPrince = 80;
	private boolean shouldBoomerang;
	private boolean isCharging;
    private static final String __OBFID = "CL_00001702";
    
    public EntityPrinceSkeleton(World worldIn)
    {
        super(worldIn);
        this.isImmuneToFire = true;
        this.isImmuneToWither = true;
        this.isInAlliedSystem = true;
        this.isCorrupted = false;
        this.tasks.addTask(2, new EntityAIArrowAttack(this, 1.0D, this.shootingSpeedPrince / 2, this.shootingSpeedPrince, 32.0F));
        this.experienceValue = 7500;
        this.setSkeletonType(0);
    }
    
    public void onUpdate()
    {
    	if (this.callSkeletonsRechargeTimer >= 0)
    	{
    		--this.callSkeletonsRechargeTimer;
    	}
    	
    	if (this.callSkeletonsRechargeTimer <= 0)
    	{
    		this.callSkeletonsRechargeTimer = 0;
    	}
    	
    	if (this.grantFireBowsTimer >= 0)
    	{
    		--this.grantFireBowsTimer;
    	}
    	
    	if (this.grantFireBowsTimer <= 0)
    	{
    		this.grantFireBowsTimer = 0;
    	}
    	
    	if (this.armorPiercingArrowRechargeTimer >= 0)
    	{
    		--this.armorPiercingArrowRechargeTimer;
    	}
    	
    	if (this.armorPiercingArrowRechargeTimer <= 0)
    	{
    		this.armorPiercingArrowRechargeTimer = 0;
    	}
    	
    	if (this.chargeTimer >= 0)
    	{
    		--this.chargeTimer;
    	}
    	
    	if (this.chargeTimer <= 0)
    	{
    		this.chargeTimer = 0;
    	}

    	this.volleyRecharge = -230;
    	this.fireBowsRecharge = -230;
    	
        super.onUpdate();
    }
    
    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(3500.0D);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(32.0D);
        this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(1.0D);
    }
    
    public IEntityLivingData func_180482_a(DifficultyInstance p_180482_1_, IEntityLivingData p_180482_2_)
    {
        this.setSkeletonType(0);
        this.setCurrentItemOrArmor(0, new ItemStack(Items.bow));
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(3500.0D);
        this.setHealth(3500.0F);
    	
        return super.func_180482_a(p_180482_1_, p_180482_2_);
    }
    
    public boolean isBurning()
    {
    	return false;
    }
    
    public void onLivingUpdate()
    {
        super.onLivingUpdate();
        
        if (!this.onGround && this.motionY > 1.2D)
        {
            this.motionY = 0.0D;
        }
        
        this.setCurrentItemOrArmor(0, new ItemStack(Items.bow));
        
        this.isCorrupted = false;
        
        if (this.rand.nextInt(120) == 0)
        {
        	this.setAttackTarget(null);
        }
        
        if (this.chargeTimer < 200)
        {
        	this.isCharging = false;
        }
        
        float brightness = this.getBrightness(1.0F);
        BlockPos blockpos = new BlockPos(this.posX, (double)Math.round(this.posY), this.posZ);
        
        if (this.worldObj.isDaytime() && !this.worldObj.isRemote)
        {
            if (brightness > 0.5F && this.worldObj.canSeeSky(blockpos))
            {
                this.shouldBoomerang = true; 
            }
        }
        else if (!this.worldObj.isDaytime() && brightness < 0.5F && this.rand.nextInt(60) == 0 && !(this.getHealth() <= 0.0F) && !this.worldObj.isRemote)
        {
            this.shouldBoomerang = false; 
            
            this.shootingSpeedPrince = 20;
        	
    		EntityLivingBase entitylivingbase = this.getAttackTarget();

    		int i = MathHelper.floor_double(this.posX);
    		int j = MathHelper.floor_double(this.posY);
    		int k = MathHelper.floor_double(this.posZ);
        
        	EntitySkeleton skeletonminion = new EntitySkeleton(this.worldObj);

            int i1 = i + MathHelper.getRandomIntegerInRange(this.rand, 4, 4) * MathHelper.getRandomIntegerInRange(this.rand, -1, 1);
            int j1 = j + MathHelper.getRandomIntegerInRange(this.rand, 4, 4) * MathHelper.getRandomIntegerInRange(this.rand, -1, 1);
            int k1 = k + MathHelper.getRandomIntegerInRange(this.rand, 4, 4) * MathHelper.getRandomIntegerInRange(this.rand, -1, 1);

            if (World.doesBlockHaveSolidTopSurface(this.worldObj, new BlockPos(i1, j1 - 1, k1)) && this.worldObj.getLightFromNeighbors(new BlockPos(i1, j1, k1)) < 10)
            {
                skeletonminion.setPosition((double)i1, (double)j1, (double)k1);

                if (this.worldObj.checkNoEntityCollision(skeletonminion.getEntityBoundingBox()) && this.worldObj.getCollidingBoundingBoxes(skeletonminion, skeletonminion.getEntityBoundingBox()).isEmpty() && !this.worldObj.isAnyLiquid(skeletonminion.getEntityBoundingBox()))
                {
                    this.worldObj.spawnEntityInWorld(skeletonminion);
                    if (entitylivingbase != null) skeletonminion.setAttackTarget(entitylivingbase);
                    skeletonminion.func_180482_a(this.worldObj.getDifficultyForLocation(new BlockPos(skeletonminion)), (IEntityLivingData)null);
                }
            }
        }
    }
    
    protected void entityInit()
    {
        super.entityInit();
        this.dataWatcher.addObject(20, new Integer(0));
    }
    
    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound tagCompound)
    {
        super.writeEntityToNBT(tagCompound);
        tagCompound.setInteger("Invul", this.getInvulTime());
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound tagCompund)
    {
        super.readEntityFromNBT(tagCompund);
        this.setInvulTime(tagCompund.getInteger("Invul"));
    }
    
    public int getInvulTime()
    {
        return this.dataWatcher.getWatchableObjectInt(20);
    }

    public void setInvulTime(int p_82215_1_)
    {
        this.dataWatcher.updateObject(20, Integer.valueOf(p_82215_1_));
    }
    
    protected void updateAITasks() 
    {
        int i;
    	
        if (this.getInvulTime() > 0)
        {
            i = this.getInvulTime() - 1;
            
            if (i <= 0)
            {
                this.worldObj.newExplosion(this, this.posX, this.posY + (double)this.getEyeHeight(), this.posZ, 7.0F, false, this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"));
                this.worldObj.playBroadcastSound(1013, new BlockPos(this), 0);
                this.playSound("bettervanilla:skeletonPrinceSpawn", 5.0F, 1.0F);
            }

            this.setInvulTime(i);

            if (this.ticksExisted % 10 == 0)
            {
                this.heal(100.0F);
            }
        }
        else
        {
            super.updateAITasks();
            
            if (this.chargeTimer <= 0)
            {
                if (this.getAttackTarget() != null && this.getAttackTarget().getDistanceSqToEntity(this) <= 6.0D * 6.0D && this.canEntityBeSeen(this.getAttackTarget()))
                {
                    if (this.onGround && this.getAttackTarget().motionX <= 0.2 && this.getAttackTarget().motionZ <= 0.2)
                    {
                        double d01 = getAttackTarget().posX - this.posX;
                        double d11 = getAttackTarget().posZ - this.posZ;
                        float f21 = MathHelper.sqrt_double(d01 * d01 + d11 * d11);
                        this.motionX = d01 / (double)f21 * 1.5D * 1.5D + this.motionX * 1.5D;
                        this.motionZ = d11 / (double)f21 * 1.5D * 1.5D + this.motionZ * 1.5D;
                        this.chargeTimer = 300;
                        this.isCharging = true;
                    }
                }
            }
            
            if (this.armorPiercingArrowRechargeTimer >= 0)
            {
                if (this.getAttackTarget() != null && this.getAttackTarget().getDistanceSqToEntity(this) <= 32.0D * 32.0D && this.canEntityBeSeen(this.getAttackTarget()))
                {
                    this.armorPiercingArrowRechargeTimer = 200;
                }
            }
            
            if (this.isCharging)
            {
                List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(1.0D, 1.0D, 1.0D));
                
                if (list != null && !list.isEmpty())
                {
                    for (int i1 = 0; i1 < list.size(); ++i1)
                    {
                        Entity entity = (Entity)list.get(i1);

                        if (entity instanceof EntityLivingBase && !entity.isInAlliedSystem && !entity.isCorrupted)
                        {
                        	entity.attackEntityFrom(DamageSource.causeMobDamage(this), 4.0F);
                        }
                    }
                }
            }
        	
            if (this.callSkeletonsRechargeTimer <= 0 && this.getAttackTarget() != null)
            {
            	this.callSkeletonsRechargeTimer = 1200;
            }
        	
            if (this.callSkeletonsRechargeTimer >= 600 && this.getAttackTarget() != null)
            {
                List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(32.0D, 32.0D, 32.0D));
                
                if (list != null && !list.isEmpty())
                {
                    for (int i1 = 0; i1 < list.size(); ++i1)
                    {
                        Entity entity = (Entity)list.get(i1);

                        if (entity instanceof EntityZombie && ((EntityZombie)entity).isZombie)
                        {
                        	((EntityZombie)entity).setAttackTarget(this.getAttackTarget());
                        }
                    }
                }
            }
            
            if (this.grantFireBowsTimer <= 0 && this.getAttackTarget() != null)
            {
            	this.grantFireBowsTimer = 400;
            	
                List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(32.0D, 32.0D, 32.0D));
                
                if (list != null && !list.isEmpty())
                {
                    for (int i1 = 0; i1 < list.size(); ++i1)
                    {
                        Entity entity = (Entity)list.get(i1);

                        if (entity instanceof EntitySkeleton)
                        {
                        	((EntitySkeleton)entity).fireBowsRecharge = 500;
                        	((EntitySkeleton)entity).playSound("bettervanilla:flamingArrows", 5.0F, 1.0F);
                        }
                    }
                }
            }
        } 
    }
    
    /**
     * Sets the Entity inside a web block.
     */
    public void setInWeb() {}
    
    public void fall(float distance, float damageMultiplier) {}
    
    protected String getTauntSound()
    {
        return this.canEntityBeSeen(this.getAttackTarget()) ? "bettervanilla:skeletonPrinceTaunt" : "bettervanilla:skeletonPrinceFindingYouTaunt";
    }
    
    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
    	if (this.rand.nextInt(5) == 0)
    	{
            this.playSound("bettervanilla:skeletonPrinceHitTaunt",this.getSoundVolume(), this.getSoundPitch());
    	}
    	
        return super.getHurtSound();
    }
    
    /**
     * Gets called every tick from main Entity class
     */
    public void onEntityUpdate()
    {
        super.onEntityUpdate();
        this.worldObj.theProfiler.startSection("tauntTicks");

        if (this.getAttackTarget() != null && this.isEntityAlive() && this.rand.nextInt(1000) < this.livingSoundTime++)
        {
            this.livingSoundTime = -this.getTalkInterval();
            this.playSound(getTauntSound(),this.getSoundVolume(), this.getSoundPitch());
        }
        
        if (this.getAttackTarget() != null && !this.canEntityBeSeen(this.getAttackTarget()) && this.shouldtaunt == false)
        {
            this.playSound("bettervanilla:skeletonPrinceLostYouTaunt",this.getSoundVolume(), this.getSoundPitch());
            this.shouldtaunt = true;
        }
        
        if (this.getAttackTarget() != null && this.canEntityBeSeen(this.getAttackTarget()) && this.shouldtaunt == true)
        {
            this.playSound("bettervanilla:skeletonPrinceFoundYouTaunt",this.getSoundVolume(), this.getSoundPitch());
            this.shouldtaunt = false;
        }
        
        if (this.deathTime == 1)
        {
            this.playSound("bettervanilla:skeletonPrinceDyingBreathTaunt",this.getSoundVolume(), this.getSoundPitch());
        }

        this.worldObj.theProfiler.endSection();
    }
    
    /**
     * Attack the specified entity using a ranged attack.
     */
    public void attackEntityWithRangedAttack(EntityLivingBase p_82196_1_, float p_82196_2_)
    {
    	if (this.shouldBoomerang)
    	{
    		EntityItem boomerang = new EntityItem(this.worldObj, this.posX , this.posY + 1.4D, this.posZ);
    		boomerang.setEntityItemStack(new ItemStack(Items.bone));
            this.playSound("random.bow", 2.0F, 0.5F / (this.getRNG().nextFloat() * 0.4F + 0.8F));
            boomerang.rotationYaw = this.rotationYawHead;
            boomerang.addVelocity((double)(-MathHelper.sin(this.rotationYawHead * (float)Math.PI / 180.0F) * 0.6F), 0.2F, (double)(-MathHelper.cos(this.rotationYawHead * (float)Math.PI / 180.0F) * 0.6F));
            this.worldObj.spawnEntityInWorld(boomerang);
            boomerang.lifespan = 40;
            boomerang.setPickupDelay(40);
            p_82196_1_.attackEntityFrom(DamageSource.causeMobDamage(this), 6.0F);
            
            if (this.rand.nextFloat() < 0.1F)
            {
                p_82196_1_.attackEntityFrom(DamageSource.magic, 6.0F);
        	}
    	}
    	else
    	{
            EntityArrow entityarrow = new EntityArrow(this.worldObj, this, p_82196_1_, 1.4F, 0.0F);
            int i = EnchantmentHelper.getEnchantmentLevel(Enchantment.power.effectId, this.getHeldItem());
            int j = EnchantmentHelper.getEnchantmentLevel(Enchantment.punch.effectId, this.getHeldItem());
            entityarrow.setDamage((double)(p_82196_2_ * 15.0F) + this.rand.nextGaussian() * 0.25D + (double)((float)this.worldObj.getDifficulty().getDifficultyId() * 0.5F));
            
            if (this.rand.nextFloat() < 0.5F)
            {
            	entityarrow.setIsCritical(true);
                entityarrow.setKnockbackStrength(1);
        	}
            
            if (this.armorPiercingArrowRechargeTimer > 80)
            {
            	entityarrow.armorPiercing = true;
            	entityarrow.setIsCritical(true);
                entityarrow.setKnockbackStrength(1);
            }
            
            if (i > 0)
            {
                entityarrow.setDamage(entityarrow.getDamage() + (double)i * 0.5D + 0.5D);
            }

            if (j > 0)
            {
                entityarrow.setKnockbackStrength(j);
            }

            if (EnchantmentHelper.getEnchantmentLevel(Enchantment.flame.effectId, this.getHeldItem()) > 0 || this.getSkeletonType() == 1)
            {
                entityarrow.setFire(100);
            }

            this.playSound("random.bow", 2.0F, 1.3F / (this.getRNG().nextFloat() * 0.4F + 0.8F));
            this.worldObj.spawnEntityInWorld(entityarrow);
    	}
    }
    
	public void func_82206_m()
	{
        this.setInvulTime(500);
        this.setHealth(this.getMaxHealth() / 4.0F);
	}
	
    /**
     * Called when the entity is attacked.
     */
    public boolean attackEntityFrom(DamageSource source, float amount)
    {
        Entity entity0;

        entity0 = source.getEntity();

        if (source == DamageSource.inFire || source == DamageSource.onFire || source == DamageSource.lava  || source == DamageSource.lightningBolt)
        {
            return false;
        }
        
        return super.attackEntityFrom(source, amount);
    }
}
