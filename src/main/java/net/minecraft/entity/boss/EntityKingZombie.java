package net.minecraft.entity.boss;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Predicate;

import net.minecraft.block.Block;
import net.minecraft.command.IEntitySelector;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.IRangedAttackMob;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIArrowAttack;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIBreakDoor;
import net.minecraft.entity.ai.EntityAIBreakDoorFast;
import net.minecraft.entity.ai.EntityAIBreakIronDoor;
import net.minecraft.entity.ai.EntityAIFleeSun;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIRestrictSun;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttribute;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.ai.attributes.RangedAttribute;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.monster.EntityGolem;
import net.minecraft.entity.monster.EntityPigZombie;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityKingZombie extends EntityZombie implements IBossDisplayData
{
    public int callZombiesRechargeTimer;
    public int diamondRechargeTimer;
	private boolean shouldtaunt;
    private static final String __OBFID = "CL_00001702";
    
    public EntityKingZombie(World worldIn)
    {
        super(worldIn);
        this.isZombie = false;
        this.isImmuneToWither = true;
        this.isInAlliedSystem = true;
        this.isCorrupted = false;
        this.isImmuneToFire = true;
        this.tasks.addTask(4, new EntityAIAttackOnCollide(this, 1.0D, false));
        this.experienceValue = 7500;
    }
    
    protected void applyEntityAI()
    {
        this.tasks.addTask(1, new EntityAIBreakDoorFast(this));
        this.tasks.addTask(1, new EntityAIBreakIronDoor(this));
        this.tasks.addTask(6, new EntityAIMoveThroughVillage(this, 1.0D, false));
        this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, true, new Class[0]));
        this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityLivingBase.class, 0, false, false, attackEntitySelector));
    }
	
    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(20.0D);
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(4000.0D);
        this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(1.0D);
    }
    
    /**
     * Applys one of 15 2 negative debuff choices to the choosen entity
     */
    protected void apply2RandomBadDebuffsTo(Entity entity)
    {
        switch (this.rand.nextInt(15))
        {
            case 0:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.confusion.id, 400, 0));
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 400, 0));
                break;
            case 1:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.blindness.id, 400, 0));
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 400, 0));
                break;
            case 2:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.hunger.id, 400, 0));
            	if (((EntityLivingBase) entity).isEntityUndead())
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.heal.id, 1, 0));
            	}
            	else
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.harm.id, 1, 0));
            	}
                break;
            case 3:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.weakness.id, 400, 0));
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.confusion.id, 400, 0));
                break;
            case 4:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.blindness.id, 400, 0));
            	if (((EntityLivingBase) entity).isEntityUndead())
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.wither.id, 1, 0));
            	}
            	else
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.poison.id, 1, 0));
            	}
                break;
            case 5:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.weakness.id, 400, 0));
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.hunger.id, 400, 0));
                break;
            case 6:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 400, 0));
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.hunger.id, 400, 0));
                break;
            case 7:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 400, 0));
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 400, 0));
                break;
            case 8:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 400, 0));
            	if (((EntityLivingBase) entity).isEntityUndead())
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.wither.id, 1, 0));
            	}
            	else
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.poison.id, 1, 0));
            	}
                break;
            case 9:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.confusion.id, 400, 0));
            	if (((EntityLivingBase) entity).isEntityUndead())
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.wither.id, 1, 0));
            	}
            	else
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.poison.id, 1, 0));
            	}
                break;
            case 10:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 400, 0));
            	if (((EntityLivingBase) entity).isEntityUndead())
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.wither.id, 1, 0));
            	}
            	else
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.poison.id, 1, 0));
            	}
                break;
            case 11:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.weakness.id, 400, 0));
            	if (((EntityLivingBase) entity).isEntityUndead())
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.wither.id, 1, 0));
            	}
            	else
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.poison.id, 1, 0));
            	}
                break;
            case 12:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.weakness.id, 400, 0));
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 400, 0));
                break;
            case 13:
            	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.weakness.id, 400, 0));
            	if (((EntityLivingBase) entity).isEntityUndead())
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.heal.id, 1, 0));
            	}
            	else
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.harm.id, 1, 0));
            	}
                break;
            case 14:
            	if (((EntityLivingBase) entity).isEntityUndead())
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.wither.id, 1, 0));
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.heal.id, 1, 0));
            	}
            	else
            	{
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.poison.id, 1, 0));
                	((EntityLivingBase)entity).addPotionEffect(new PotionEffect(Potion.harm.id, 1, 0));
            	}
        }
    }
    
    /**
     * Returns the current armor value as determined by a call to InventoryPlayer.getTotalArmorValue
     */
    public int getTotalArmorValue()
    {
        return 15;
    }
    
    /**
     * Sets the Entity inside a web block.
     */
    public void setInWeb() {}
    
    public void fall(float distance, float damageMultiplier) {}
    
    protected String getTauntSound()
    {
        return this.canEntityBeSeen(this.getAttackTarget()) ? "bettervanilla:zombieKingTaunt" : "bettervanilla:zombieKingFindingYouTaunt";
    }
    
    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
    	if (this.rand.nextInt(5) == 0)
    	{
            this.playSound("bettervanilla:zombieKingHitTaunt",this.getSoundVolume(), this.getSoundPitch());
    	}
    	
        return "mob.zombie.hurt";
    }
    
    /**
     * Gets called every tick from main Entity class
     */
    public void onEntityUpdate()
    {
        super.onEntityUpdate();
        this.worldObj.theProfiler.startSection("tauntTicks");

        if (this.getAttackTarget() != null && this.isEntityAlive() && this.rand.nextInt(1000) < this.livingSoundTime++)
        {
            this.livingSoundTime = -this.getTalkInterval();
            this.playSound(getTauntSound(),this.getSoundVolume(), this.getSoundPitch());
        }
        
        if (this.getAttackTarget() != null && !this.canEntityBeSeen(this.getAttackTarget()) && this.shouldtaunt == false)
        {
            this.playSound("bettervanilla:zombieKingLostYouTaunt",this.getSoundVolume(), this.getSoundPitch());
            this.shouldtaunt = true;
        }
        
        if (this.getAttackTarget() != null && this.canEntityBeSeen(this.getAttackTarget()) && this.shouldtaunt == true)
        {
            this.playSound("bettervanilla:zombieKingFoundYouTaunt",this.getSoundVolume(), this.getSoundPitch());
            this.shouldtaunt = false;
        }
        
        if (this.deathTime == 1)
        {
            this.playSound("bettervanilla:zombieKingDyingBreathTaunt",this.getSoundVolume(), this.getSoundPitch());
        }

        this.worldObj.theProfiler.endSection();
    }
    
    public boolean attackEntityAsMob(Entity p_70652_1_)
    {
        float f = (float)this.getEntityAttribute(SharedMonsterAttributes.attackDamage).getAttributeValue();
    	
        if (super.attackEntityAsMob(p_70652_1_))
        {
        	if (this.rand.nextInt(5) == 0)
        	{
                this.playSound("bettervanilla:zombieKingHitTaunt",this.getSoundVolume(), this.getSoundPitch());
        	}
        	
        	if (p_70652_1_ instanceof EntityGolem)
        	{
        		p_70652_1_.attackEntityFrom(DamageSource.causeMobDamage(this), f * 15);
        	}
        	
        	return true;
        }
        else
        {
        	return false;
        }
        
    }
    
    public void onLivingUpdate()
    {
        super.onLivingUpdate();
        
        if (!this.onGround && this.motionY > 1.2D)
        {
            this.motionY = 0.0D;
        }
        
        this.isCorrupted = false;
        
        if (this.getInvulTime() <= 0)
        {
            this.setCurrentItemOrArmor(0, new ItemStack(Items.golden_sword));
            this.setCurrentItemOrArmor(1, new ItemStack(Items.golden_helmet));
            this.setCurrentItemOrArmor(2, new ItemStack(Items.golden_chestplate));
            this.setCurrentItemOrArmor(3, new ItemStack(Items.golden_leggings));
            this.setCurrentItemOrArmor(4, new ItemStack(Items.golden_boots));
        }
        
        if (this.rand.nextInt(120) == 0)
        {
        	this.setAttackTarget(null);
        }
        
        float brightness = this.getBrightness(1.0F);
        BlockPos blockpos = new BlockPos(this.posX, (double)Math.round(this.posY), this.posZ);
        
        if (this.worldObj.isDaytime() && !this.worldObj.isRemote)
        {
            if (brightness > 0.5F && this.rand.nextInt(400) == 0 && this.worldObj.canSeeSky(blockpos))
            {
                this.playSound("bettervanilla:zombieKingRoar", 10.0F, this.getSoundPitch());
                
                List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(64.0D, 64.0D, 64.0D));
                
                if (list != null && !list.isEmpty())
                {
                    for (int i = 0; i < list.size(); ++i)
                    {
                        Entity entity = (Entity)list.get(i);

                        if (entity instanceof EntityLivingBase && !entity.isInAlliedSystem)
                        {
                        	this.apply2RandomBadDebuffsTo(entity);
                        }
                    }
                }
            }
        }
        else if (!this.worldObj.isDaytime() && brightness < 0.5F && this.rand.nextInt(60) == 0 && !(this.getHealth() <= 0.0F) && !this.worldObj.isRemote)
        {
    		EntityLivingBase entitylivingbase = this.getAttackTarget();

    		int i = MathHelper.floor_double(this.posX);
    		int j = MathHelper.floor_double(this.posY);
    		int k = MathHelper.floor_double(this.posZ);
        
        	EntityZombie entityzombie = this.createInstance();

            int i1 = i + MathHelper.getRandomIntegerInRange(this.rand, 4, 4) * MathHelper.getRandomIntegerInRange(this.rand, -1, 1);
            int j1 = j + MathHelper.getRandomIntegerInRange(this.rand, 4, 4) * MathHelper.getRandomIntegerInRange(this.rand, -1, 1);
            int k1 = k + MathHelper.getRandomIntegerInRange(this.rand, 4, 4) * MathHelper.getRandomIntegerInRange(this.rand, -1, 1);

            if (World.doesBlockHaveSolidTopSurface(this.worldObj, new BlockPos(i1, j1 - 1, k1)) && this.worldObj.getLightFromNeighbors(new BlockPos(i1, j1, k1)) < 10)
            {
                entityzombie.setPosition((double)i1, (double)j1, (double)k1);

                if (this.worldObj.checkNoEntityCollision(entityzombie.getEntityBoundingBox()) && this.worldObj.getCollidingBoundingBoxes(entityzombie, entityzombie.getEntityBoundingBox()).isEmpty() && !this.worldObj.isAnyLiquid(entityzombie.getEntityBoundingBox()))
                {
                    this.worldObj.spawnEntityInWorld(entityzombie);
                    if (entitylivingbase != null) entityzombie.setAttackTarget(entitylivingbase);
                    entityzombie.func_180482_a(this.worldObj.getDifficultyForLocation(new BlockPos(entityzombie)), (IEntityLivingData)null);
                    entityzombie.specialRechargeTimer = -720;
                }
            }
        }
    }
    
    protected void addRandomArmor()
    {
        switch (this.rand.nextInt(6))
        {
            case 0:
                this.dropItem(Items.iron_ingot, 1);
                break;
            case 1:
                this.dropItem(Items.carrot, 1);
                break;
            case 2:
                this.dropItem(Items.potato, 1);
                break;
            case 3:
                this.dropItem(Items.gold_ingot, 1);
                break;
            case 4:
                this.dropItem(Items.diamond, 1);
                break;
            case 5:
                this.dropItem(Items.apple, 1);
        }
    }
    
    public void setDead()
    {
        this.isDead = true;
    }
    
    /**
     * Makes the entity despawn if requirements are reached
     */
    protected void despawnEntity()
    {
        this.entityAge = 0;
    }
    
    public void onUpdate()
    {
    	if (this.callZombiesRechargeTimer <= 0)
    	{
    		++this.callZombiesRechargeTimer;
    	}
    	
    	if (this.callZombiesRechargeTimer >= 0)
    	{
    		this.callZombiesRechargeTimer = 0;
    	}
    	
    	if (this.diamondRechargeTimer <= 0)
    	{
    		++this.diamondRechargeTimer;
    	}
    	
    	if (this.diamondRechargeTimer >= 0)
    	{
    		this.diamondRechargeTimer = 0;
    	}

        super.onUpdate();
    }
    
    protected void updateAITasks() 
    {
        int i;
    	
        if (this.getInvulTime() > 0)
        {
            i = this.getInvulTime() - 1;
            
            if (i == 400)
            {
                this.setCurrentItemOrArmor(4, new ItemStack(Items.golden_boots));
            }
            
            if (i == 300)
            {
                this.setCurrentItemOrArmor(3, new ItemStack(Items.golden_leggings));
            }
            
            if (i == 200)
            {
                this.setCurrentItemOrArmor(2, new ItemStack(Items.golden_chestplate));
            }
            
            if (i == 100)
            {
                this.setCurrentItemOrArmor(1, new ItemStack(Items.golden_helmet));
            }

            if (i <= 0)
            {
                this.worldObj.newExplosion(this, this.posX, this.posY + (double)this.getEyeHeight(), this.posZ, 7.0F, false, this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"));
                this.worldObj.playBroadcastSound(1013, new BlockPos(this), 0);
                this.playSound("bettervanilla:zombieKingSpawn", 5.0F, 1.0F);
            }

            this.setInvulTime(i);

            if (this.ticksExisted % 10 == 0)
            {
                this.heal(100.0F);
            }
        }
        else
        {
            super.updateAITasks();
        	
            if (this.callZombiesRechargeTimer >= 0 && this.getAttackTarget() != null)
            {
            	this.callZombiesRechargeTimer = -1200;
            }
        	
            if (this.callZombiesRechargeTimer <= -600 && this.getAttackTarget() != null)
            {
                List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(32.0D, 32.0D, 32.0D));
                
                if (list != null && !list.isEmpty())
                {
                    for (int i1 = 0; i1 < list.size(); ++i1)
                    {
                        Entity entity = (Entity)list.get(i1);

                        if (entity instanceof EntityZombie && ((EntityZombie)entity).isZombie)
                        {
                        	((EntityZombie)entity).setAttackTarget(this.getAttackTarget());
                        }
                    }
                }
            }
            
            if (this.diamondRechargeTimer >= 0 && this.getAttackTarget() != null)
            {
            	this.diamondRechargeTimer = -800;
            	
                List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(16.0D, 16.0D, 16.0D));
                
                if (list != null && !list.isEmpty())
                {
                    for (int i1 = 0; i1 < list.size(); ++i1)
                    {
                        Entity entity = (Entity)list.get(i1);

                        if (entity instanceof EntityZombie && ((EntityZombie)entity).isZombie)
                        {
                        	((EntityZombie)entity).setCurrentItemOrArmor(0, new ItemStack(Items.diamond_sword));
                        	((EntityZombie)entity).setCurrentItemOrArmor(4, new ItemStack(Items.diamond_helmet));
                        	((EntityZombie)entity).diamondEquipRechargeTimer = -800;
                        }
                    }
                }
            }
        }
    }
    
    protected void entityInit()
    {
        super.entityInit();
        this.dataWatcher.addObject(20, new Integer(0));
    }
    
    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound tagCompound)
    {
        super.writeEntityToNBT(tagCompound);
        tagCompound.setInteger("Invul", this.getInvulTime());
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound tagCompund)
    {
        super.readEntityFromNBT(tagCompund);
        this.setInvulTime(tagCompund.getInteger("Invul"));
    }
    
    public int getInvulTime()
    {
        return this.dataWatcher.getWatchableObjectInt(20);
    }

    public void setInvulTime(int p_82215_1_)
    {
        this.dataWatcher.updateObject(20, Integer.valueOf(p_82215_1_));
    }

	public void func_82206_m()
	{
        this.setInvulTime(500);
        this.setHealth(this.getMaxHealth() / 4.0F);
	}
}
