package net.minecraft.entity.projectile;

import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.EntityDragon;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.boss.EntityWitherGood;
import net.minecraft.entity.monster.EntityWitherMinion;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.Explosion;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityWitherSkull extends EntityFireball
{
    public int explosionPower = 1;
    public int ajustableDamage = 8;
	public boolean paladinStrike;
    private static final String __OBFID = "CL_00001728";

    public EntityWitherSkull(World worldIn)
    {
        super(worldIn);
        this.setSize(0.5F, 0.5F);
    }

    public EntityWitherSkull(World worldIn, EntityLivingBase p_i1794_2_, double p_i1794_3_, double p_i1794_5_, double p_i1794_7_)
    {
        super(worldIn, p_i1794_2_, p_i1794_3_, p_i1794_5_, p_i1794_7_);
        this.setSize(0.5F, 0.5F);
    }

    /**
     * Return the motion factor for this projectile. The factor is multiplied by the original motion.
     */
    protected float getMotionFactor()
    {
        return this.isInvulnerable() ? 0.73F : super.getMotionFactor();
    }

    @SideOnly(Side.CLIENT)
    public EntityWitherSkull(World worldIn, double p_i1795_2_, double p_i1795_4_, double p_i1795_6_, double p_i1795_8_, double p_i1795_10_, double p_i1795_12_)
    {
        super(worldIn, p_i1795_2_, p_i1795_4_, p_i1795_6_, p_i1795_8_, p_i1795_10_, p_i1795_12_);
        this.setSize(0.3125F, 0.3125F);
    }

    /**
     * Returns true if the entity is on fire. Used by render to add the fire effect on rendering.
     */
    public boolean isBurning()
    {
        return false;
    }

    /**
     * Explosion resistance of a block relative to this entity
     */
    public float getExplosionResistance(Explosion p_180428_1_, World worldIn, BlockPos p_180428_3_, IBlockState p_180428_4_)
    {
        float f = super.getExplosionResistance(p_180428_1_, worldIn, p_180428_3_, p_180428_4_);

        if (this.isInvulnerable() && p_180428_4_.getBlock() != Blocks.bedrock && p_180428_4_.getBlock() != Blocks.end_portal && p_180428_4_.getBlock() != Blocks.end_portal_frame && p_180428_4_.getBlock() != Blocks.command_block)
        {
            f = Math.min(0.8F, f);
        }

        return f;
    }

    /**
     * Called when this EntityFireball hits a block or entity.
     */
    protected void onImpact(MovingObjectPosition movingObject)
    {
        if (!this.worldObj.isRemote)
        {
            if (movingObject.entityHit != null && !(movingObject.entityHit instanceof EntityWither))
            {
                if (this.shootingEntity != null)
                {
                    if (movingObject.entityHit.attackEntityFrom(DamageSource.causeMobDamage(this.shootingEntity), (float)this.ajustableDamage))
                    {
                        this.shootingEntity.heal(1.0F);
                    	
                    	if (movingObject.entityHit.height < 6.0F && !(movingObject.entityHit instanceof EntityDragon) && !(movingObject.entityHit instanceof EntityWitherGood) && !(movingObject.entityHit instanceof EntityWither) && !(movingObject.entityHit instanceof EntityWitherMinion) && !(movingObject.entityHit instanceof EntityPlayer))
                    	{
                    		movingObject.entityHit.isCorrupted = true;
                    	}
                    	
                        if (!movingObject.entityHit.isEntityAlive())
                        {
                            this.shootingEntity.heal(5.0F);
                        }
                        else
                        {
                            this.func_174815_a(this.shootingEntity, movingObject.entityHit);
                        }
                    }
                }
                else
                {
                    movingObject.entityHit.attackEntityFrom(DamageSource.magic, (float)this.ajustableDamage);
                }

                if (movingObject.entityHit instanceof EntityLivingBase)
                {
                    byte b0 = 0;

                    if (this.worldObj.getDifficulty() == EnumDifficulty.NORMAL)
                    {
                        b0 = 10;
                    }
                    else if (this.worldObj.getDifficulty() == EnumDifficulty.HARD)
                    {
                        b0 = 40;
                    }

                    if (b0 > 0)
                    {
                        ((EntityLivingBase)movingObject.entityHit).addPotionEffect(new PotionEffect(Potion.wither.id, 20 * b0, 1));
                    }
                }
            }

            if (this.isInvulnerable())
            {
                this.worldObj.newExplosion(this, this.posX, this.posY, this.posZ, (float)this.explosionPower * 2, false, this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"));
            }
            else
            {
                this.worldObj.newExplosion(this, this.posX, this.posY, this.posZ, (float)this.explosionPower, false, this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"));
            }
            this.setDead();
        }
    }
    
    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound tagCompound)
    {
        super.writeEntityToNBT(tagCompound);
        tagCompound.setInteger("ExplosionPower", this.explosionPower);
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound tagCompund)
    {
        super.readEntityFromNBT(tagCompund);

        if (tagCompund.hasKey("ExplosionPower", 99))
        {
            this.explosionPower = tagCompund.getInteger("ExplosionPower");
        }
    }

    /**
     * Called when the entity is attacked.
     */
    public boolean attackEntityFrom(DamageSource source, float amount)
    {
        Entity entity = source.getSourceOfDamage();

        if (entity instanceof EntityArrow)
        {
            if (this.isInvulnerable())
            {
                this.worldObj.newExplosion(this, this.posX, this.posY, this.posZ, (float)this.explosionPower * 2, false, this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"));
            }
            else
            {
                this.worldObj.newExplosion(this, this.posX, this.posY, this.posZ, (float)this.explosionPower, false, this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"));
            }
            this.setDead();
            entity.setDead();
            return false;
        }
    	
        return false;
    }

    protected void entityInit()
    {
        this.dataWatcher.addObject(10, Byte.valueOf((byte)0));
    }

    /**
     * Return whether this skull comes from an invulnerable (aura) wither boss.
     */
    public boolean isInvulnerable()
    {
        return this.dataWatcher.getWatchableObjectByte(10) == 1;
    }

    /**
     * Set whether this skull comes from an invulnerable (aura) wither boss.
     */
    public void setInvulnerable(boolean p_82343_1_)
    {
        this.dataWatcher.updateObject(10, Byte.valueOf((byte)(p_82343_1_ ? 1 : 0)));
    }
}
