package net.minecraft.entity.projectile;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityBlaze;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

public class EntitySnowGolemSnowball extends EntitySnowball
{
    private static final String __OBFID = "CL_00001722";

    public EntitySnowGolemSnowball(World worldIn)
    {
        super(worldIn);
    }

    public EntitySnowGolemSnowball(World worldIn, EntityLivingBase p_i1774_2_)
    {
        super(worldIn, p_i1774_2_);
    }

    public EntitySnowGolemSnowball(World worldIn, double x, double y, double z)
    {
        super(worldIn, x, y, z);
    }

    /**
     * Called when this EntityThrowable hits a block or entity.
     */
    protected void onImpact(MovingObjectPosition p_70184_1_)
    {
        if (p_70184_1_.entityHit != null)
        {
            float b0 = 2.0F;

            if (p_70184_1_.entityHit instanceof EntityBlaze)
            {
                b0 = b0 * 10.0F;
                
                this.playSound("random.fizz", 1.0F, 1.0F);
                this.playSound("random.fizz", 1.0F, 1.0F);
                this.playSound("random.fizz", 1.0F, 1.0F);
                this.playSound("random.fizz", 1.0F, 1.0F);
                this.playSound("random.fizz", 1.0F, 1.0F);
                
                for (int i = 0; i < 48; ++i)
                {
                    this.worldObj.spawnParticle(EnumParticleTypes.SNOWBALL, this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D, new int[0]);
                }
            }

            p_70184_1_.entityHit.attackEntityFrom(DamageSource.causeThrownDamage(this, this.getThrower()), b0);
        }

        for (int i = 0; i < 8; ++i)
        {
            this.worldObj.spawnParticle(EnumParticleTypes.SNOWBALL, this.posX, this.posY, this.posZ, 0.0D, 0.0D, 0.0D, new int[0]);
        }

        if (!this.worldObj.isRemote)
        {
            this.setDead();
        }
    }
}
