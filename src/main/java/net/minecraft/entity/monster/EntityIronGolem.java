package net.minecraft.entity.monster;

import java.util.List;

import com.google.common.base.Predicate;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFlower;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIDefendVillage;
import net.minecraft.entity.ai.EntityAIGuardVillager;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookAtVillager;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAIMoveTowardsTarget;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.boss.EntityWither;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.pathfinding.PathNavigateGround;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MathHelper;
import net.minecraft.village.Village;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;
import net.minecraft.world.WorldProviderEnd;
import net.minecraft.world.WorldServer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class EntityIronGolem extends EntityGolem
{
    /** deincrements, and a distance-to-home check is done at 0 */
    private int homeCheckTimer;
    Village villageObj;
    private int attackTimer;
    private int holdRoseTick;
    public int specialRechargeTimer;
    private static final String __OBFID = "CL_00001652";

    public EntityIronGolem(World worldIn)
    {
        super(worldIn);
        double d;
        if (this.reaction > 0)
        {
            d = 0.5D;
        }
        else
        {
            d = 1.0D;
        }
        this.setSize(1.75F, 4.0F);
        this.isImmuneToFire = true;
        ((PathNavigateGround)this.getNavigator()).func_179690_a(true);
        this.tasks.addTask(1, new EntityAIAttackOnCollide(this, d, true));
        this.tasks.addTask(2, new EntityAIGuardVillager(this, 0.6D));
        this.tasks.addTask(3, new EntityAIMoveThroughVillage(this, 1.0D, true));
        this.tasks.addTask(4, new EntityAIMoveTowardsRestriction(this, 1.0D));
        this.tasks.addTask(5, new EntityAILookAtVillager(this));
        this.tasks.addTask(6, new EntityAIWander(this, 0.6D));
        this.tasks.addTask(7, new EntityAIWatchClosest(this, EntityPlayer.class, 6.0F));
        this.tasks.addTask(8, new EntityAILookIdle(this));
        this.targetTasks.addTask(1, new EntityAIDefendVillage(this));
        this.targetTasks.addTask(2, new EntityAIHurtByTarget(this, false, new Class[0]));
        this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityLivingBase.class, 0, false, false, attackEntitySelector));
        if (this.isCorrupted)
        {
            this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityLivingBase.class, 0, false, false, attackEntitySelector2));
        }
    }
    
    public boolean isInLava()
    {
        return this.worldObj.isMaterialInBB(this.getEntityBoundingBox(), Material.lava);
    }
    
    private static final Predicate attackEntitySelector = new Predicate()
    {
        private static final String __OBFID = "CL_00001662";
        
        public boolean func_180027_a(Entity p_180027_1_)
        {
            return p_180027_1_ instanceof IMob || p_180027_1_.isCorrupted;
        }
        public boolean apply(Object p_apply_1_)
        {
            return this.func_180027_a((Entity)p_apply_1_);
        }
    };
    
    private static final Predicate attackEntitySelector2 = new Predicate()
    {
        private static final String __OBFID = "CL_00001662";
        
        public boolean func_180027_a(Entity p_180027_1_)
        {
            return p_180027_1_ instanceof EntityLivingBase && !(p_180027_1_.isCorrupted);
        }
        public boolean apply(Object p_apply_1_)
        {
            return this.func_180027_a((Entity)p_apply_1_);
        }
    };

    protected void entityInit()
    {
        super.entityInit();
        this.dataWatcher.addObject(16, Byte.valueOf((byte)0));
    }
    
    public void fall(float distance, float damageMultiplier)
    {
        float[] ret = net.minecraftforge.common.ForgeHooks.onLivingFall(this, distance, damageMultiplier);
        if (ret == null) return;
        distance = ret[0]; damageMultiplier = ret[1];
        PotionEffect potioneffect = this.getActivePotionEffect(Potion.jump);
        float f2 = potioneffect != null ? (float)(potioneffect.getAmplifier() + 1) : 0.0F;
        int i = MathHelper.ceiling_float_int((distance - 8.0F - f2) * damageMultiplier);

        if (i > 0)
        {
            this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
            this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
            this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
            this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
            
        	this.destroyBlocksInAABB(this.getEntityBoundingBox().expand(0.5D, 1.0D, 0.5D));
            
            EntityPlayer entityplayer = this.worldObj.getClosestPlayerToEntity(this, 24.0D);

            if (entityplayer != null)
            {
                this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
                this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
                this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
                this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
            	
                for (int s = 0; s < 500; ++s)
                {
                	entityplayer.rotationPitch = entityplayer.rotationPitch + this.rand.nextInt(2);
                	entityplayer.rotationPitch = entityplayer.rotationPitch - this.rand.nextInt(2);
                }
            }
            
            List list11 = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(3.0D, 1.0D, 3.0D));
            
            if (list11 != null && !list11.isEmpty())
            {
                for (int i1 = 0; i1 < list11.size(); ++i1)
                {
                    Entity entity = (Entity)list11.get(i1);
                	
                	if (!(entity instanceof EntityIronGolem) && !(entity instanceof EntityVillager) && !(entity instanceof EntityGuard) && !(entity instanceof EntityGuardFemale) && !(entity instanceof EntityHumanArcher) && !(entity instanceof EntityKnight))
                	{
                    	entity.attackEntityFrom(DamageSource.anvil, 50.0F);
                    	
                        double d0 = (this.getEntityBoundingBox().minX + this.getEntityBoundingBox().maxX);
                        double d1 = (this.getEntityBoundingBox().minZ + this.getEntityBoundingBox().maxZ);
                        double d2 = entity.posX - d0;
                        double d3 = entity.posZ - d1;
                        double d4 = d2 * d2 + d3 * d3;
                        entity.addVelocity(d2 / d4 * 4.0D, 3.0D, d3 / d4 * 4.0D);
                	}
                }
            }
            
            int j = MathHelper.floor_double(this.posX);
            int k = MathHelper.floor_double(this.posY - 0.20000000298023224D);
            int l = MathHelper.floor_double(this.posZ);
            Block block = this.worldObj.getBlockState(new BlockPos(j, k, l)).getBlock();

            if (block.getMaterial() != Material.air)
            {
                Block.SoundType soundtype = block.stepSound;
                this.playSound(soundtype.getStepSound(), soundtype.getVolume() * 0.5F, soundtype.getFrequency() * 0.75F);
            }
        }
    }
    
    protected void func_180433_a(double p_180433_1_, boolean p_180433_3_, Block p_180433_4_, BlockPos p_180433_5_)
    {
        if (!this.isInWater())
        {
            this.handleWaterMovement();
        }

        if (!this.worldObj.isRemote && this.fallDistance > 3.0F && p_180433_3_)
        {
            IBlockState iblockstate = this.worldObj.getBlockState(p_180433_5_);
            Block block1 = iblockstate.getBlock();
            float f = (float)MathHelper.ceiling_float_int(this.fallDistance - 3.0F);

            if (block1.getMaterial() != Material.air)
            {
                double d1 = (double)Math.min(0.2F + f / 15.0F, 10.0F);

                if (d1 > 5.0D)
                {
                    d1 = 5.0D;
                }

                int i = (int)(5000.0D * d1);
                ((WorldServer)this.worldObj).spawnParticle(EnumParticleTypes.BLOCK_DUST, this.posX, this.posY, this.posZ, i, 0.0D, 0.0D, 0.0D, 0.15000000596046448D, new int[] {Block.getStateId(iblockstate)});
            }
        }

        super.func_180433_a(p_180433_1_, p_180433_3_, p_180433_4_, p_180433_5_);
    }
    
    /**
     * Destroys all blocks that aren't associated with 'The End' inside the given bounding box.
     */
    private boolean destroyBlocksInAABB(AxisAlignedBB p_70972_1_)
    {
        int i = MathHelper.floor_double(p_70972_1_.minX);
        int j = MathHelper.floor_double(p_70972_1_.minY);
        int k = MathHelper.floor_double(p_70972_1_.minZ);
        int l = MathHelper.floor_double(p_70972_1_.maxX);
        int i1 = MathHelper.floor_double(p_70972_1_.maxY);
        int j1 = MathHelper.floor_double(p_70972_1_.maxZ);
        boolean flag = false;
        boolean flag1 = false;

        for (int k1 = i; k1 <= l; ++k1)
        {
            for (int l1 = j; l1 <= i1; ++l1)
            {
                for (int i2 = k; i2 <= j1; ++i2)
                {
                    Block block = this.worldObj.getBlockState(new BlockPos(k1, l1, i2)).getBlock();
                    
                    Block blockextra = this.worldObj.getBlockState(new BlockPos(k1, l1 - 1, i2)).getBlock();

                    if (!block.isAir(worldObj, new BlockPos(k1, l1, i2)))
                    {
                        if (block.getExplosionResistance(this) < 10.0F && this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"))
                        {
                            flag1 = this.worldObj.destroyBlock(new BlockPos(k1, l1, i2), true) || flag1;
                            
                            if (!blockextra.isAir(worldObj, new BlockPos(k1, l1 - 1, i2)))
                            {
                                if (blockextra.getExplosionResistance(this) < 1.2F && this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"))
                                {
                                    flag1 = this.worldObj.destroyBlock(new BlockPos(k1, l1 - 1, i2), true) || flag1;
                                }
                            }
                        }
                        else
                        {
                            flag = true;
                        }
                    }
                }
            }
        }

        if (flag1)
        {
            double d1 = p_70972_1_.minX + (p_70972_1_.maxX - p_70972_1_.minX) * (double)this.rand.nextFloat();
            double d2 = p_70972_1_.minY + (p_70972_1_.maxY - p_70972_1_.minY) * (double)this.rand.nextFloat();
            double d0 = p_70972_1_.minZ + (p_70972_1_.maxZ - p_70972_1_.minZ) * (double)this.rand.nextFloat();
            this.worldObj.spawnParticle(EnumParticleTypes.EXPLOSION_LARGE, d1, d2, d0, 0.0D, 0.0D, 0.0D, new int[0]);
        }

        return flag;
    }

    protected void updateAITasks()
    {
    	if (this.motionY < -0.9F)
    	{
            List list11 = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(1.0D, 1.0D, 1.0D));
            
            if (list11 != null && !list11.isEmpty())
            {
                for (int i = 0; i < list11.size(); ++i)
                {
                    Entity entity = (Entity)list11.get(i);
                    
                	if (!(entity instanceof EntityIronGolem) && !(entity instanceof EntityVillager) && !(entity instanceof EntityGuard) && !(entity instanceof EntityGuardFemale) && !(entity instanceof EntityHumanArcher) && !(entity instanceof EntityKnight))
                	{
                    	entity.attackEntityFrom(DamageSource.anvil, 10.0F);
                	}
                }
            }
    	}
    	
        if (--this.homeCheckTimer <= 0)
        {
            this.homeCheckTimer = 70 + this.rand.nextInt(50);
            this.villageObj = this.worldObj.getVillageCollection().getNearestVillage(new BlockPos(this), 32);

            if (this.villageObj == null)
            {
                this.detachHome();
            }
            else
            {
                BlockPos blockpos = this.villageObj.getCenter();
                this.func_175449_a(blockpos, (int)((float)this.villageObj.getVillageRadius() * 0.6F));
            }
        }
        
        if (this.attackTimer == 21)
        {
            List list11 = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(12.0D, 1.0D, 12.0D));
            
            if (list11 != null && !list11.isEmpty())
            {
                for (int i1 = 0; i1 < list11.size(); ++i1)
                {
                    Entity entity = (Entity)list11.get(i1);
                	
                	if (!(entity instanceof EntityIronGolem) && !(entity instanceof EntityVillager) && !(entity instanceof EntitySnowman) && !(entity instanceof EntityGuard) && !(entity instanceof EntityGuardFemale) && !(entity instanceof EntityHumanArcher) && !(entity instanceof EntityKnight))
                	{
                    	entity.attackEntityFrom(DamageSource.causeMobDamage(this), 9 + this.rand.nextInt(3));
                    	
                        this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
                        this.playSound("bettervanilla:groundSmash", 5.0F, 1.0F);
                    	
                        double d0 = (this.getEntityBoundingBox().minX + this.getEntityBoundingBox().maxX);
                        double d1 = (this.getEntityBoundingBox().minZ + this.getEntityBoundingBox().maxZ);
                        double d2 = entity.posX - d0;
                        double d3 = entity.posZ - d1;
                        double d4 = d2 * d2 + d3 * d3;
                        entity.addVelocity(d2 / d4 * 1.0D, 3.0D, d3 / d4 * 1.0D);
                	}
                }
            }
        }
        
        if (this.specialRechargeTimer >= 0)
        {
            if (this.getAttackTarget() != null && this.getAttackTarget().getDistanceSqToEntity(this) <= 12.0D * 12.0D)
            {
            	this.attackTimer = 30;
            	this.specialRechargeTimer = -400;
            }
        }

        super.updateAITasks();
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.knockbackResistance).setBaseValue(1.0D);
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(4000.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.25D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(200.0D);
    }
    
    /**
     * Returns the current armor value as determined by a call to InventoryPlayer.getTotalArmorValue
     */
    public int getTotalArmorValue()
    {
        return 20;
    }

    /**
     * Decrements the entity's air supply when underwater
     */
    protected int decreaseAirSupply(int p_70682_1_)
    {
        return p_70682_1_;
    }

    protected void collideWithEntity(Entity p_82167_1_)
    {
        if (p_82167_1_ instanceof IMob)
        {
            this.setAttackTarget((EntityLivingBase)p_82167_1_);
        }

        super.collideWithEntity(p_82167_1_);
    }

    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    public void onLivingUpdate()
    {
        super.onLivingUpdate();
        
    	if (this.isInLava())
        {
            this.attackEntityFrom(DamageSource.lava, 500.0F);
        }

        if (this.attackTimer > 0)
        {
            --this.attackTimer;
        }

        if (this.holdRoseTick > 0)
        {
            --this.holdRoseTick;
        }

        if (this.motionX * this.motionX + this.motionZ * this.motionZ > 2.500000277905201E-7D && this.rand.nextInt(2) == 0)
        {
            int i = MathHelper.floor_double(this.posX);
            int j = MathHelper.floor_double(this.posY - 0.20000000298023224D);
            int k = MathHelper.floor_double(this.posZ);
            IBlockState iblockstate = this.worldObj.getBlockState(new BlockPos(i, j, k));
            Block block = iblockstate.getBlock();

            if (block.getMaterial() != Material.air)
            {
                this.playSound("random.explode", 0.075F, 0.5F);
                this.worldObj.spawnParticle(EnumParticleTypes.BLOCK_CRACK, this.posX + ((double)this.rand.nextFloat() - 0.5D) * (double)this.width, this.getEntityBoundingBox().minY + 0.1D, this.posZ + ((double)this.rand.nextFloat() - 0.5D) * (double)this.width, 4.0D * ((double)this.rand.nextFloat() - 0.5D), 0.5D, ((double)this.rand.nextFloat() - 0.5D) * 4.0D, new int[] {Block.getStateId(iblockstate)});
            }
            
            EntityPlayer entityplayer = this.worldObj.getClosestPlayerToEntity(this, 12.0D);

            if (entityplayer != null)
            {
                for (int s = 0; s < 5; ++s)
                {
                	entityplayer.rotationPitch = entityplayer.rotationPitch + this.rand.nextInt(2);
                	entityplayer.rotationPitch = entityplayer.rotationPitch - this.rand.nextInt(2);
                }
            }
        }
    }

    /**
     * Returns true if this entity can attack entities of the specified class.
     */
    public boolean canAttackClass(Class p_70686_1_)
    {
        return this.isPlayerCreated() && EntityPlayer.class.isAssignableFrom(p_70686_1_) ? false : super.canAttackClass(p_70686_1_);
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound tagCompound)
    {
        super.writeEntityToNBT(tagCompound);
        tagCompound.setBoolean("PlayerCreated", this.isPlayerCreated());
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound tagCompund)
    {
        super.readEntityFromNBT(tagCompund);
        this.setPlayerCreated(tagCompund.getBoolean("PlayerCreated"));
    }

    public boolean attackEntityAsMob(Entity p_70652_1_)
    {
        float f = (float)this.getEntityAttribute(SharedMonsterAttributes.attackDamage).getAttributeValue();
    	
        this.attackTimer = 10;
        this.worldObj.setEntityState(this, (byte)4);
        boolean flag = p_70652_1_.attackEntityFrom(DamageSource.causeMobDamage(this), f);

        if (flag)
        {
            p_70652_1_.motionY += 5.0D;
            this.func_174815_a(this, p_70652_1_);
        }

        this.playSound("mob.irongolem.throw", 1.0F, 1.0F);
        return flag;
    }

    @SideOnly(Side.CLIENT)
    public void handleHealthUpdate(byte p_70103_1_)
    {
        if (p_70103_1_ == 4)
        {
            this.attackTimer = 10;
            this.playSound("mob.irongolem.throw", 1.0F, 1.0F);
        }
        else if (p_70103_1_ == 11)
        {
            this.holdRoseTick = 400;
        }
        else
        {
            super.handleHealthUpdate(p_70103_1_);
        }
    }

    public Village getVillage()
    {
        return this.villageObj;
    }

    @SideOnly(Side.CLIENT)
    public int getAttackTimer()
    {
        return this.attackTimer;
    }

    public void setHoldingRose(boolean p_70851_1_)
    {
        this.holdRoseTick = p_70851_1_ ? 400 : 0;
        this.worldObj.setEntityState(this, (byte)11);
    }

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "mob.irongolem.hit";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "mob.irongolem.death";
    }

    protected void playStepSound(BlockPos p_180429_1_, Block p_180429_2_)
    {
        this.playSound("mob.irongolem.walk", 1.0F, 1.0F);
    }
    
    /**
     * Returns the volume for the sounds this mob makes.
     */
    protected float getSoundVolume()
    {
        return 2.0F;
    }

    /**
     * Drop 0-2 items of this living's type
     */
    protected void dropFewItems(boolean p_70628_1_, int p_70628_2_)
    {
        int j = this.rand.nextInt(3);
        int k;

        for (k = 0; k < j; ++k)
        {
            this.dropItemWithOffset(Item.getItemFromBlock(Blocks.red_flower), 1, (float)BlockFlower.EnumFlowerType.POPPY.getMeta());
        }

        k = 3 + this.rand.nextInt(3);

        for (int l = 0; l < k; ++l)
        {
            this.dropItem(Items.iron_ingot, 1);
        }
    }

    public int getHoldRoseTick()
    {
        return this.holdRoseTick;
    }

    public boolean isPlayerCreated()
    {
        return (this.dataWatcher.getWatchableObjectByte(16) & 1) != 0;
    }

    public void setPlayerCreated(boolean p_70849_1_)
    {
        byte b0 = this.dataWatcher.getWatchableObjectByte(16);

        if (p_70849_1_)
        {
            this.dataWatcher.updateObject(16, Byte.valueOf((byte)(b0 | 1)));
        }
        else
        {
            this.dataWatcher.updateObject(16, Byte.valueOf((byte)(b0 & -2)));
        }
    }

    /**
     * Called when the mob's health reaches 0.
     */
    public void onDeath(DamageSource cause)
    {
        if (!this.isPlayerCreated() && this.attackingPlayer != null && this.villageObj != null)
        {
            this.villageObj.setReputationForPlayer(this.attackingPlayer.getName(), -50);
        }

        super.onDeath(cause);
    }
    
    public boolean attackEntityFrom(DamageSource source, float p_70097_2_)
    {
        if (this.isEntityInvulnerable(source))
        {
            return false;
        }
        else
        {
            Entity entity0;

            entity0 = source.getEntity();

            if (entity0 != null && entity0 instanceof EntityHumanArcher && !entity0.isCorrupted)
            {
                return false;
            }
            
            Entity entity = source.getSourceOfDamage();

            if (entity instanceof EntityArrow)
            {
                this.playSound("bettervanilla:arrowDeflect", 1.0F, this.getSoundPitch());
                return false;
            }
            
            if (entity0 != null && entity0 instanceof EntitySnowman && !entity0.isCorrupted)
            {
            	this.heal(2);
                double d0 = this.rand.nextGaussian() * 0.02D;
                double d1 = this.rand.nextGaussian() * 0.02D;
                double d2 = this.rand.nextGaussian() * 0.02D;
                this.worldObj.spawnParticle(EnumParticleTypes.HEART, this.posX + (double)(this.rand.nextFloat() * this.width * 2.0F) - (double)this.width, this.posY + 0.5D + (double)(this.rand.nextFloat() * this.height), this.posZ + (double)(this.rand.nextFloat() * this.width * 2.0F) - (double)this.width, d0, d1, d2, new int[0]);
                return false;
            }
        }
        
        return super.attackEntityFrom(source, p_70097_2_);
    }
    
    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
    	if (this.specialRechargeTimer <= 0)
    	{
    		++this.specialRechargeTimer;
    	}
    	
    	if (this.specialRechargeTimer >= 0)
    	{
    		this.specialRechargeTimer = 0;
    	}

        super.onUpdate();
    }
}