package net.minecraft.entity.monster;

import com.google.common.base.Predicate;
import com.google.common.collect.Sets;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIBase;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.EntityDamageSourceIndirect;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class EntityEndermanBeta extends EntityMob
{
    private static final Set carriableBlocks = Sets.newIdentityHashSet();
    private boolean isAggressive;
	private int randomSoundDelay;
    public int specialRechargeTimer;
    private static final String __OBFID = "CL_00001685";

    private static final Predicate attackEntitySelector = new Predicate()
    {
        private static final String __OBFID = "CL_00001662";
        
        public boolean func_180027_a(Entity p_180027_1_)
        {
            return p_180027_1_ instanceof EntityLivingBase && !p_180027_1_.isInAlliedSystem && !(p_180027_1_ instanceof EntityPlayer) || p_180027_1_.isCorrupted;
        }
        public boolean apply(Object p_apply_1_)
        {
            return this.func_180027_a((Entity)p_apply_1_);
        }
    };
    
    private static final Predicate attackEntitySelector2 = new Predicate()
    {
        private static final String __OBFID = "CL_00001662";
        
        public boolean func_180027_a(Entity p_180027_1_)
        {
            return p_180027_1_ instanceof EntityLivingBase && !(p_180027_1_.isCorrupted);
        }
        public boolean apply(Object p_apply_1_)
        {
            return this.func_180027_a((Entity)p_apply_1_);
        }
    };
    
    public EntityEndermanBeta(World worldIn)
    {
        super(worldIn);
        if (this.isCorrupted)
        {
            this.isInAlliedSystem = false;
        }
        else
        {
            this.isInAlliedSystem = true;
        }
        this.setSize(0.5F, 3.0F);
        this.stepHeight = 1.0F;
        this.tasks.addTask(0, new EntityAISwimming(this));
        this.tasks.addTask(2, new EntityAIAttackOnCollide(this, EntityLivingBase.class, 1.75D, false));
        this.tasks.addTask(7, new EntityAIWander(this, 0.75D));
        this.tasks.addTask(8, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        this.tasks.addTask(8, new EntityAILookIdle(this));
        this.tasks.addTask(10, new EntityEndermanBeta.AIPlaceBlock());
        this.tasks.addTask(11, new EntityEndermanBeta.AITakeBlock());
        this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, false, new Class[0]));
        this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityLivingBase.class, 0, false, false, attackEntitySelector));
        if (this.isCorrupted)
        {
            this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityLivingBase.class, 0, false, false, attackEntitySelector2));
        }
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(40.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.25D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(7.0D);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(64.0D);
    }

    protected void entityInit()
    {
        super.entityInit();
        this.dataWatcher.addObject(16, new Short((short)0));
        this.dataWatcher.addObject(17, new Byte((byte)0));
        this.dataWatcher.addObject(18, new Byte((byte)0));
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound tagCompound)
    {
        super.writeEntityToNBT(tagCompound);
        IBlockState iblockstate = this.func_175489_ck();
        tagCompound.setShort("carried", (short)Block.getIdFromBlock(iblockstate.getBlock()));
        tagCompound.setShort("carriedData", (short)iblockstate.getBlock().getMetaFromState(iblockstate));
    }

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound tagCompund)
    {
        super.readEntityFromNBT(tagCompund);
        IBlockState iblockstate;

        if (tagCompund.hasKey("carried", 8))
        {
            iblockstate = Block.getBlockFromName(tagCompund.getString("carried")).getStateFromMeta(tagCompund.getShort("carriedData") & 65535);
        }
        else
        {
            iblockstate = Block.getBlockById(tagCompund.getShort("carried")).getStateFromMeta(tagCompund.getShort("carriedData") & 65535);
        }

        this.func_175490_a(iblockstate);
    }

    /**
     * Finds the closest player within 64 blocks looking at this entity
     */
    protected Entity findPlayerToLookAt()
    {
        EntityPlayer entityplayer = this.worldObj.getClosestPlayerToEntity(this, 64.0D);

        if (entityplayer != null)
        {
            if (this.isPlayerRegistered(entityplayer) && !entityplayer.capabilities.disableDamage && this.getAttackTarget() == null)
            {
                this.setScreaming(true);
                this.setAttackTarget(entityplayer);
        		this.faceEntity(entityplayer, 180, 180);
            }
        }

        return super.findPlayerToLookAt();
    }
    
    /**
     * Checks to see if this entity can see this player
     */
    protected boolean isPlayerRegistered(EntityPlayer p_70821_1_)
    {
        ItemStack itemstack = p_70821_1_.inventory.armorInventory[3];

        if (itemstack != null && itemstack.getItem() == Item.getItemFromBlock(Blocks.pumpkin))
        {
            return false;
        }
        else
        {
            Vec3 vec3 = p_70821_1_.getLook(1.0F).normalize();
            Vec3 vec31 = new Vec3(this.posX - p_70821_1_.posX, this.getEntityBoundingBox().minY + this.getEyeHeight() - (p_70821_1_.posY + (double)p_70821_1_.getEyeHeight()), this.posZ - p_70821_1_.posZ);
            double d0 = vec31.lengthVector();
            vec31 = vec31.normalize();
            double d1 = vec3.dotProduct(vec31);
            return d1 > 1.0D - 0.025D / d0 ? p_70821_1_.canEntityBeSeen(this) : false;
        }
    }

    public float getEyeHeight()
    {
        return 2.55F;
    }

    /**
     * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
     * use this to react to sunlight and start to burn.
     */
    public void onLivingUpdate()
    {
        if (this.worldObj.isRemote)
        {
            for (int i = 0; i < 2; ++i)
            {
                this.worldObj.spawnParticle(EnumParticleTypes.SMOKE_LARGE, this.posX + (this.rand.nextDouble() - 0.5D) * (double)this.width, this.posY + this.rand.nextDouble() * (double)this.height, this.posZ + (this.rand.nextDouble() - 0.5D) * (double)this.width, 0.0D, 0.0D, 0.0D, new int[0]);
            }
        }
        
        if (this.getAttackTarget() != null)
        {
            this.setScreaming(true);
        }
        
        if (this.worldObj.isDaytime() && !this.worldObj.isRemote && !this.isChild())
        {
            float f = this.getBrightness(1.0F);
            BlockPos blockpos = new BlockPos(this.posX, (double)Math.round(this.posY), this.posZ);

            if (f > 0.5F && this.rand.nextFloat() * 30.0F < (f - 0.4F) * 2.0F && this.worldObj.canSeeSky(blockpos))
            {
                boolean flag = true;
                ItemStack itemstack = this.getEquipmentInSlot(4);

                if (itemstack != null)
                {
                    if (itemstack.isItemStackDamageable())
                    {
                        itemstack.setItemDamage(itemstack.getItemDamage() + this.rand.nextInt(2));

                        if (itemstack.getItemDamage() >= itemstack.getMaxDamage())
                        {
                            this.renderBrokenItemStack(itemstack);
                            this.setCurrentItemOrArmor(4, (ItemStack)null);
                        }
                    }

                    flag = false;
                }

                if (flag)
                {
                    this.setFire(8);
                }
            }
        }

        this.isJumping = false;
        
        if (this.getAttackTarget() != null && this.getRNG().nextInt(5) == 0)
        {
            if (this.getAttackTarget().getDistanceSqToEntity(this) < 2.0D && !this.worldObj.isRemote)
            {
                this.teleportRandomly();
            }
            
            if (this.getAttackTarget().getDistanceSqToEntity(this) > 16.0D && !this.worldObj.isRemote)
            {
                this.teleportToEntity(this.getAttackTarget());
            }
        }
        
        super.onLivingUpdate();
    }

    protected void updateAITasks()
    {
    	
        if (this.isWet())
        {
            this.attackEntityFrom(DamageSource.drown, 1.0F);
        }

        if (this.isScreaming() && !this.isAggressive && this.rand.nextInt(100) == 0)
        {
            this.setScreaming(false);
        }

        super.updateAITasks();
    }

    /**
     * Teleport the enderman to a random nearby position
     */
    protected boolean teleportRandomly()
    {
        double d0 = this.posX + (this.rand.nextDouble() - 0.5D) * 64.0D;
        double d1 = this.posY + (double)(this.rand.nextInt(64) - 32);
        double d2 = this.posZ + (this.rand.nextDouble() - 0.5D) * 64.0D;
        return this.teleportTo(d0, d1, d2);
    }

    /**
     * Teleport the enderman to another entity
     */
    protected boolean teleportToEntity(Entity p_70816_1_)
    {
        Vec3 vec3 = new Vec3(this.posX - p_70816_1_.posX, this.getEntityBoundingBox().minY + (double)(this.height / 2.0F) - p_70816_1_.posY + (double)p_70816_1_.getEyeHeight(), this.posZ - p_70816_1_.posZ);
        vec3 = vec3.normalize();
        double d0 = 16.0D;
        double d1 = this.posX + (this.rand.nextDouble() - 0.5D) * 8.0D - vec3.xCoord * d0;
        double d2 = this.posY + (double)(this.rand.nextInt(16) - 8) - vec3.yCoord * d0;
        double d3 = this.posZ + (this.rand.nextDouble() - 0.5D) * 8.0D - vec3.zCoord * d0;
        return this.teleportTo(d1, d2, d3);
    }

    /**
     * Teleport the enderman
     */
    protected boolean teleportTo(double p_70825_1_, double p_70825_3_, double p_70825_5_)
    {
        net.minecraftforge.event.entity.living.EnderTeleportEvent event = new net.minecraftforge.event.entity.living.EnderTeleportEvent(this, p_70825_1_, p_70825_3_, p_70825_5_, 0);
        if (net.minecraftforge.common.MinecraftForge.EVENT_BUS.post(event)) return false;
        double d3 = this.posX;
        double d4 = this.posY;
        double d5 = this.posZ;
        this.posX = event.targetX;
        this.posY = event.targetY;
        this.posZ = event.targetZ;
        boolean flag = false;
        BlockPos blockpos = new BlockPos(this.posX, this.posY, this.posZ);

        if (this.worldObj.isBlockLoaded(blockpos))
        {
            boolean flag1 = false;

            while (!flag1 && blockpos.getY() > 0)
            {
                BlockPos blockpos1 = blockpos.down();
                Block block = this.worldObj.getBlockState(blockpos1).getBlock();

                if (block.getMaterial().blocksMovement())
                {
                    flag1 = true;
                }
                else
                {
                    --this.posY;
                    blockpos = blockpos1;
                }
            }

            if (flag1)
            {
                super.setPositionAndUpdate(this.posX, this.posY, this.posZ);

                if (this.worldObj.getCollidingBoundingBoxes(this, this.getEntityBoundingBox()).isEmpty() && !this.worldObj.isAnyLiquid(this.getEntityBoundingBox()))
                {
                    flag = true;
                }
            }
        }

        if (!flag)
        {
            this.setPosition(d3, d4, d5);
            return false;
        }
        else
        {
            short short1 = 128;

            for (int i = 0; i < short1; ++i)
            {
                double d9 = (double)i / ((double)short1 - 1.0D);
                float f = (this.rand.nextFloat() - 0.5F) * 0.2F;
                float f1 = (this.rand.nextFloat() - 0.5F) * 0.2F;
                float f2 = (this.rand.nextFloat() - 0.5F) * 0.2F;
                double d6 = d3 + (this.posX - d3) * d9 + (this.rand.nextDouble() - 0.5D) * (double)this.width * 2.0D;
                double d7 = d4 + (this.posY - d4) * d9 + this.rand.nextDouble() * (double)this.height;
                double d8 = d5 + (this.posZ - d5) * d9 + (this.rand.nextDouble() - 0.5D) * (double)this.width * 2.0D;
                this.worldObj.spawnParticle(EnumParticleTypes.SMOKE_LARGE, d6, d7, d8, (double)f, (double)f1, (double)f2, new int[0]);
            }
            
            return true;
        }
    }

    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound()
    {
        return "mob.zombie.say";
    }

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "mob.zombie.hurt";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "mob.zombie.death";
    }

    protected Item getDropItem()
    {
        return Items.diamond;
    }

    /**
     * Drop 0-2 items of this living's type
     */
    protected void dropFewItems(boolean p_70628_1_, int p_70628_2_)
    {
        Item item = this.getDropItem();

        if (item != null)
        {
            int j = this.rand.nextInt(2 + p_70628_2_);

            for (int k = 0; k < j; ++k)
            {
                this.dropItem(item, 1);
            }
        }
    }

    public void func_175490_a(IBlockState p_175490_1_)
    {
        this.dataWatcher.updateObject(16, Short.valueOf((short)(Block.getStateId(p_175490_1_) & 65535)));
    }

    public IBlockState func_175489_ck()
    {
        return Block.getStateById(this.dataWatcher.getWatchableObjectShort(16) & 65535);
    }

    /**
     * Called when the entity is attacked.
     */
    public boolean attackEntityFrom(DamageSource source, float amount)
    {
        if (this.isEntityInvulnerable(source))
        {
            return false;
        }
        else
        {
            Entity entity0;

            entity0 = source.getEntity();

            if (entity0 != null && entity0.isInAlliedSystem && this.isInAlliedSystem && !this.isCorrupted && !entity0.isCorrupted)
            {
                return false;
            }
            
            Entity entity = source.getEntity();
            
        	if(entity instanceof Entity && source.getSourceOfDamage() instanceof EntityLivingBase)
        	{
                List list = this.worldObj.getEntitiesWithinAABBExcludingEntity(this, this.getEntityBoundingBox().expand(16.0D, 16.0D, 16.0D));

                for (int i = 0; i < list.size(); ++i)
                {
                    Entity entity1 = (Entity)list.get(i);

                    if (entity1 instanceof EntityEndermanBeta)
                    {
                    	EntityEndermanBeta entityguardian = (EntityEndermanBeta)entity1;
                        this.setAttackTarget((EntityLivingBase) entity);
                        this.randomSoundDelay = this.rand.nextInt(40);
                        entityguardian.setAttackTarget((EntityLivingBase) entity);
                        entityguardian.randomSoundDelay = entityguardian.rand.nextInt(40);
                    }
                }
        	}
        	
            if (source.getEntity() == null || !(source.getEntity() instanceof EntityEndermite))
            {
                if (!this.worldObj.isRemote)
                {
                    this.setScreaming(true);
                }

                if (source instanceof EntityDamageSource && source.getEntity() instanceof EntityPlayer)
                {
                    if (source.getEntity() instanceof EntityPlayerMP && ((EntityPlayerMP)source.getEntity()).theItemInWorldManager.isCreative())
                    {
                        this.setScreaming(false);
                    }
                    else
                    {
                        this.isAggressive = true;
                    }
                }
            }

            boolean flag = super.attackEntityFrom(source, amount);

            return flag;
        }
    }

    public boolean isScreaming()
    {
        return this.dataWatcher.getWatchableObjectByte(18) > 0;
    }

    public void setScreaming(boolean p_70819_1_)
    {
        this.dataWatcher.updateObject(18, Byte.valueOf((byte)(p_70819_1_ ? 1 : 0)));
    }

    static
    {
        carriableBlocks.add(Blocks.stone);
        carriableBlocks.add(Blocks.grass);
        carriableBlocks.add(Blocks.dirt);
        carriableBlocks.add(Blocks.cobblestone);
        carriableBlocks.add(Blocks.planks);
        carriableBlocks.add(Blocks.sapling);
        carriableBlocks.add(Blocks.sand);
        carriableBlocks.add(Blocks.gravel);
        carriableBlocks.add(Blocks.gold_ore);
        carriableBlocks.add(Blocks.iron_ore);
        carriableBlocks.add(Blocks.coal_ore);
        carriableBlocks.add(Blocks.log);
        carriableBlocks.add(Blocks.log2);
        carriableBlocks.add(Blocks.leaves);
        carriableBlocks.add(Blocks.leaves2);
        carriableBlocks.add(Blocks.sponge);
        carriableBlocks.add(Blocks.glass);
        carriableBlocks.add(Blocks.lapis_ore);
        carriableBlocks.add(Blocks.lapis_block);
        carriableBlocks.add(Blocks.dispenser);
        carriableBlocks.add(Blocks.sandstone);
        carriableBlocks.add(Blocks.noteblock);
        carriableBlocks.add(Blocks.golden_rail);
        carriableBlocks.add(Blocks.detector_rail);
        carriableBlocks.add(Blocks.web);
        carriableBlocks.add(Blocks.tallgrass);
        carriableBlocks.add(Blocks.deadbush);
        carriableBlocks.add(Blocks.wool);
        carriableBlocks.add(Blocks.yellow_flower);
        carriableBlocks.add(Blocks.red_flower);
        carriableBlocks.add(Blocks.brown_mushroom);
        carriableBlocks.add(Blocks.red_mushroom);
        carriableBlocks.add(Blocks.gold_block);
        carriableBlocks.add(Blocks.iron_block);
        carriableBlocks.add(Blocks.double_stone_slab);
        carriableBlocks.add(Blocks.stone_slab);
        carriableBlocks.add(Blocks.brick_block);
        carriableBlocks.add(Blocks.tnt);
        carriableBlocks.add(Blocks.bookshelf);
        carriableBlocks.add(Blocks.mossy_cobblestone);
        carriableBlocks.add(Blocks.obsidian);
        carriableBlocks.add(Blocks.torch);
        carriableBlocks.add(Blocks.fire);
        carriableBlocks.add(Blocks.oak_stairs);
        carriableBlocks.add(Blocks.chest);
        carriableBlocks.add(Blocks.redstone_wire);
        carriableBlocks.add(Blocks.diamond_ore);
        carriableBlocks.add(Blocks.diamond_block);
        carriableBlocks.add(Blocks.crafting_table);
        carriableBlocks.add(Blocks.wheat);
        carriableBlocks.add(Blocks.farmland);
        carriableBlocks.add(Blocks.furnace);
        carriableBlocks.add(Blocks.lit_furnace);
        carriableBlocks.add(Blocks.standing_sign);
        carriableBlocks.add(Blocks.ladder);
        carriableBlocks.add(Blocks.rail);
        carriableBlocks.add(Blocks.stone_stairs);
        carriableBlocks.add(Blocks.wall_sign);
        carriableBlocks.add(Blocks.stone_pressure_plate);
        carriableBlocks.add(Blocks.wooden_pressure_plate);
        carriableBlocks.add(Blocks.redstone_ore);
        carriableBlocks.add(Blocks.lit_redstone_ore);
        carriableBlocks.add(Blocks.unlit_redstone_torch);
        carriableBlocks.add(Blocks.redstone_torch);
        carriableBlocks.add(Blocks.stone_button);
        carriableBlocks.add(Blocks.snow_layer);
        carriableBlocks.add(Blocks.ice);
        carriableBlocks.add(Blocks.snow);
        carriableBlocks.add(Blocks.cactus);
        carriableBlocks.add(Blocks.clay);
        carriableBlocks.add(Blocks.reeds);
        carriableBlocks.add(Blocks.jukebox);
        carriableBlocks.add(Blocks.oak_fence);
        carriableBlocks.add(Blocks.spruce_fence);
        carriableBlocks.add(Blocks.birch_fence);
        carriableBlocks.add(Blocks.jungle_fence);
        carriableBlocks.add(Blocks.dark_oak_fence);
        carriableBlocks.add(Blocks.acacia_fence);
        carriableBlocks.add(Blocks.pumpkin);
        carriableBlocks.add(Blocks.netherrack);
        carriableBlocks.add(Blocks.soul_sand);
        carriableBlocks.add(Blocks.glowstone);
        carriableBlocks.add(Blocks.portal);
        carriableBlocks.add(Blocks.lit_pumpkin);
        carriableBlocks.add(Blocks.cake);
        carriableBlocks.add(Blocks.unpowered_repeater);
        carriableBlocks.add(Blocks.powered_repeater);
        carriableBlocks.add(Blocks.trapdoor);
        carriableBlocks.add(Blocks.monster_egg);
        carriableBlocks.add(Blocks.stonebrick);
        carriableBlocks.add(Blocks.brown_mushroom_block);
        carriableBlocks.add(Blocks.red_mushroom_block);
        carriableBlocks.add(Blocks.iron_bars);
        carriableBlocks.add(Blocks.glass_pane);
        carriableBlocks.add(Blocks.melon_block);
        carriableBlocks.add(Blocks.pumpkin_stem);
        carriableBlocks.add(Blocks.melon_stem);
        carriableBlocks.add(Blocks.vine);
        carriableBlocks.add(Blocks.oak_fence_gate);
        carriableBlocks.add(Blocks.spruce_fence_gate);
        carriableBlocks.add(Blocks.birch_fence_gate);
        carriableBlocks.add(Blocks.jungle_fence_gate);
        carriableBlocks.add(Blocks.dark_oak_fence_gate);
        carriableBlocks.add(Blocks.acacia_fence_gate);
        carriableBlocks.add(Blocks.brick_stairs);
        carriableBlocks.add(Blocks.stone_brick_stairs);
        carriableBlocks.add(Blocks.mycelium);
        carriableBlocks.add(Blocks.waterlily);
        carriableBlocks.add(Blocks.nether_brick);
        carriableBlocks.add(Blocks.nether_brick_fence);
        carriableBlocks.add(Blocks.nether_brick_stairs);
        carriableBlocks.add(Blocks.nether_wart);
        carriableBlocks.add(Blocks.enchanting_table);
        carriableBlocks.add(Blocks.brewing_stand);
        carriableBlocks.add(Blocks.cauldron);
        carriableBlocks.add(Blocks.end_stone);
        carriableBlocks.add(Blocks.redstone_lamp);
        carriableBlocks.add(Blocks.lit_redstone_lamp);
        carriableBlocks.add(Blocks.double_wooden_slab);
        carriableBlocks.add(Blocks.wooden_slab);
        carriableBlocks.add(Blocks.cocoa);
        carriableBlocks.add(Blocks.sandstone_stairs);
        carriableBlocks.add(Blocks.emerald_ore);
        carriableBlocks.add(Blocks.ender_chest);
        carriableBlocks.add(Blocks.tripwire_hook);
        carriableBlocks.add(Blocks.tripwire);
        carriableBlocks.add(Blocks.emerald_block);
        carriableBlocks.add(Blocks.spruce_stairs);
        carriableBlocks.add(Blocks.birch_stairs);
        carriableBlocks.add(Blocks.jungle_stairs);
        carriableBlocks.add(Blocks.cobblestone_wall);
        carriableBlocks.add(Blocks.flower_pot);
        carriableBlocks.add(Blocks.carrots);
        carriableBlocks.add(Blocks.potatoes);
        carriableBlocks.add(Blocks.wooden_button);
        carriableBlocks.add(Blocks.skull);
        carriableBlocks.add(Blocks.anvil);
        carriableBlocks.add(Blocks.trapped_chest);
        carriableBlocks.add(Blocks.light_weighted_pressure_plate);
        carriableBlocks.add(Blocks.heavy_weighted_pressure_plate);
        carriableBlocks.add(Blocks.unpowered_comparator);
        carriableBlocks.add(Blocks.powered_comparator);
        carriableBlocks.add(Blocks.daylight_detector);
        carriableBlocks.add(Blocks.daylight_detector_inverted);
        carriableBlocks.add(Blocks.redstone_block);
        carriableBlocks.add(Blocks.quartz_ore);
        carriableBlocks.add(Blocks.hopper);
        carriableBlocks.add(Blocks.quartz_block);
        carriableBlocks.add(Blocks.quartz_stairs);
        carriableBlocks.add(Blocks.activator_rail);
        carriableBlocks.add(Blocks.dropper);
        carriableBlocks.add(Blocks.stained_hardened_clay);
        carriableBlocks.add(Blocks.iron_trapdoor);
        carriableBlocks.add(Blocks.hay_block);
        carriableBlocks.add(Blocks.carpet);
        carriableBlocks.add(Blocks.hardened_clay);
        carriableBlocks.add(Blocks.coal_block);
        carriableBlocks.add(Blocks.packed_ice);
        carriableBlocks.add(Blocks.acacia_stairs);
        carriableBlocks.add(Blocks.dark_oak_stairs);
        carriableBlocks.add(Blocks.slime_block);
        carriableBlocks.add(Blocks.double_plant);
        carriableBlocks.add(Blocks.stained_glass);
        carriableBlocks.add(Blocks.stained_glass_pane);
        carriableBlocks.add(Blocks.prismarine);
        carriableBlocks.add(Blocks.sea_lantern);
        carriableBlocks.add(Blocks.standing_banner);
        carriableBlocks.add(Blocks.wall_banner);
        carriableBlocks.add(Blocks.red_sandstone);
        carriableBlocks.add(Blocks.red_sandstone_stairs);
        carriableBlocks.add(Blocks.double_stone_slab2);
        carriableBlocks.add(Blocks.stone_slab2);
    }

    class AIPlaceBlock extends EntityAIBase
    {
        private EntityEndermanBeta field_179475_a = EntityEndermanBeta.this;
        private static final String __OBFID = "CL_00002222";

        /**
         * Returns whether the EntityAIBase should begin execution.
         */
        public boolean shouldExecute()
        {
            return !this.field_179475_a.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing") ? false : (this.field_179475_a.func_175489_ck().getBlock().getMaterial() == Material.air ? false : this.field_179475_a.getRNG().nextInt(2000) == 0);
        }

        /**
         * Updates the task
         */
        public void updateTask()
        {
            Random random = this.field_179475_a.getRNG();
            World world = this.field_179475_a.worldObj;
            int i = MathHelper.floor_double(this.field_179475_a.posX - 1.0D + random.nextDouble() * 2.0D);
            int j = MathHelper.floor_double(this.field_179475_a.posY + random.nextDouble() * 2.0D);
            int k = MathHelper.floor_double(this.field_179475_a.posZ - 1.0D + random.nextDouble() * 2.0D);
            BlockPos blockpos = new BlockPos(i, j, k);
            Block block = world.getBlockState(blockpos).getBlock();
            Block block1 = world.getBlockState(blockpos.down()).getBlock();

            if (this.func_179474_a(world, blockpos, this.field_179475_a.func_175489_ck().getBlock(), block, block1))
            {
                world.setBlockState(blockpos, this.field_179475_a.func_175489_ck(), 3);
                this.field_179475_a.func_175490_a(Blocks.air.getDefaultState());
            }
        }

        private boolean func_179474_a(World worldIn, BlockPos p_179474_2_, Block p_179474_3_, Block p_179474_4_, Block p_179474_5_)
        {
            return !p_179474_3_.canPlaceBlockAt(worldIn, p_179474_2_) ? false : (p_179474_4_.getMaterial() != Material.air ? false : (p_179474_5_.getMaterial() == Material.air ? false : p_179474_5_.isFullCube()));
        }
    }

    class AITakeBlock extends EntityAIBase
    {
        private EntityEndermanBeta field_179473_a = EntityEndermanBeta.this;
        private static final String __OBFID = "CL_00002220";

        /**
         * Returns whether the EntityAIBase should begin execution.
         */
        public boolean shouldExecute()
        {
            return !this.field_179473_a.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing") ? false : (this.field_179473_a.func_175489_ck().getBlock().getMaterial() != Material.air ? false : this.field_179473_a.getRNG().nextInt(20) == 0);
        }

        /**
         * Updates the task
         */
        public void updateTask()
        {
            Random random = this.field_179473_a.getRNG();
            World world = this.field_179473_a.worldObj;
            int i = MathHelper.floor_double(this.field_179473_a.posX - 2.0D + random.nextDouble() * 4.0D);
            int j = MathHelper.floor_double(this.field_179473_a.posY + random.nextDouble() * 3.0D);
            int k = MathHelper.floor_double(this.field_179473_a.posZ - 2.0D + random.nextDouble() * 4.0D);
            BlockPos blockpos = new BlockPos(i, j, k);
            IBlockState iblockstate = world.getBlockState(blockpos);
            Block block = iblockstate.getBlock();

            if (EntityEndermanBeta.carriableBlocks.contains(block))
            {
                this.field_179473_a.func_175490_a(iblockstate);
                world.setBlockState(blockpos, Blocks.air.getDefaultState());
            }
        }
    }

    /*===================================== Forge Start ==============================*/
    public static void setCarriable(Block block, boolean canCarry)
    {
        if (canCarry) carriableBlocks.add(block);
        else          carriableBlocks.remove(block);
    }
    public static boolean getCarriable(Block block)
    {
        return carriableBlocks.contains(block);
    }
    /*===================================== Forge End ==============================*/
}