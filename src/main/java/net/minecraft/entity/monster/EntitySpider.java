package net.minecraft.entity.monster;

import java.util.List;
import java.util.Random;

import com.google.common.base.Predicate;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIAttackOnCollide;
import net.minecraft.entity.ai.EntityAIBreakDoor;
import net.minecraft.entity.ai.EntityAIFleeSun;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAILeapAtTarget;
import net.minecraft.entity.ai.EntityAILookIdle;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAIMoveTowardsRestriction;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIRestrictSun;
import net.minecraft.entity.ai.EntityAISwimming;
import net.minecraft.entity.ai.EntityAIWander;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.pathfinding.PathNavigateClimber;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.BlockPos;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.util.Vec3;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

public class EntitySpider extends EntityMob
{
    private static final String __OBFID = "CL_00001699";

    private static final Predicate attackEntitySelector = new Predicate()
    {
        private static final String __OBFID = "CL_00001662";
        
        public boolean func_180027_a(Entity p_180027_1_)
        {
            return p_180027_1_ instanceof EntityLivingBase && !p_180027_1_.isInAlliedSystem || p_180027_1_.isCorrupted;
        }
        public boolean apply(Object p_apply_1_)
        {
            return this.func_180027_a((Entity)p_apply_1_);
        }
    };
    
    private static final Predicate attackEntitySelector2 = new Predicate()
    {
        private static final String __OBFID = "CL_00001662";
        
        public boolean func_180027_a(Entity p_180027_1_)
        {
            return p_180027_1_ instanceof EntityLivingBase && !(p_180027_1_.isCorrupted);
        }
        public boolean apply(Object p_apply_1_)
        {
            return this.func_180027_a((Entity)p_apply_1_);
        }
    };
    
    public int specialRechargeTimer;
    
    public EntitySpider(World worldIn)
    {
        super(worldIn);
        if (this.isCorrupted)
        {
            this.isInAlliedSystem = false;
        }
        else
        {
            this.isInAlliedSystem = true;
        }
        this.setSize(1.75F, 0.78F);
        this.tasks.addTask(1, new EntityAISwimming(this));
        this.tasks.addTask(3, new EntityAILeapAtTarget(this, 0.5F));
        this.tasks.addTask(4, new EntitySpider.AISpiderAttack(EntityLivingBase.class));
        this.tasks.addTask(5, new EntityAIWander(this, 0.4D));
        this.tasks.addTask(6, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        this.tasks.addTask(6, new EntityAILookIdle(this));
        this.tasks.addTask(2, new EntityAIRestrictSun(this));
        this.tasks.addTask(7, new EntityAIFleeSun(this, 0.4D));
        this.tasks.addTask(5, new EntityAIMoveTowardsRestriction(this, 1.0D));
        this.tasks.addTask(6, new EntityAIMoveThroughVillage(this, 1.0D, false));
        this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, false, new Class[0]));
        this.targetTasks.addTask(2, new EntitySpider.AISpiderTarget(this, EntityLivingBase.class, 0, false, false, attackEntitySelector));
        
        if (this.isCorrupted)
        {
            this.targetTasks.addTask(2, new EntitySpider.AISpiderTarget(this, EntityLivingBase.class, 0, false, false, attackEntitySelector2));
        }
    }

    protected PathNavigate func_175447_b(World worldIn)
    {
        return new PathNavigateClimber(this, worldIn);
    }

    protected void entityInit()
    {
        super.entityInit();
        this.dataWatcher.addObject(16, new Byte((byte)0));
    }

    /**
     * Called to update the entity's position/logic.
     */
    public void onUpdate()
    {
        super.onUpdate();

        if (!this.worldObj.isRemote)
        {
            this.setBesideClimbableBlock(this.isCollidedHorizontally);
        }
        
    	if (this.specialRechargeTimer <= 0)
    	{
    		++this.specialRechargeTimer;
    	}
    	
    	if (this.specialRechargeTimer >= 0)
    	{
    		this.specialRechargeTimer = 0;
    	}
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(24.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.5D);
        this.getEntityAttribute(SharedMonsterAttributes.followRange).setBaseValue(32.0D);
    }

    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound()
    {
        return "mob.spider.say";
    }

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return "mob.spider.say";
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return "mob.spider.death";
    }

    protected void playStepSound(BlockPos p_180429_1_, Block p_180429_2_)
    {
        this.playSound("mob.spider.step", 0.5F, 1.0F);
    }

    protected Item getDropItem()
    {
        return Items.string;
    }

    /**
     * Drop 0-2 items of this living's type
     */
    protected void dropFewItems(boolean p_70628_1_, int p_70628_2_)
    {
        super.dropFewItems(p_70628_1_, p_70628_2_);

        if (p_70628_1_ && (this.rand.nextInt(3) == 0 || this.rand.nextInt(1 + p_70628_2_) > 0))
        {
            this.dropItem(Items.spider_eye, 1);
        }
    }

    /**
     * returns true if this entity is by a ladder, false otherwise
     */
    public boolean isOnLadder()
    {
        return this.isBesideClimbableBlock();
    }

    /**
     * Sets the Entity inside a web block.
     */
    public void setInWeb() {}

    /**
     * Get this Entity's EnumCreatureAttribute
     */
    public EnumCreatureAttribute getCreatureAttribute()
    {
        return EnumCreatureAttribute.ARTHROPOD;
    }

    public boolean isPotionApplicable(PotionEffect p_70687_1_)
    {
        return p_70687_1_.getPotionID() == Potion.poison.id ? false : super.isPotionApplicable(p_70687_1_);
    }

    /**
     * Returns true if the WatchableObject (Byte) is 0x01 otherwise returns false. The WatchableObject is updated using
     * setBesideClimableBlock.
     */
    public boolean isBesideClimbableBlock()
    {
        return (this.dataWatcher.getWatchableObjectByte(16) & 1) != 0;
    }

    /**
     * Updates the WatchableObject (Byte) created in entityInit(), setting it to 0x01 if par1 is true or 0x00 if it is
     * false.
     */
    public void setBesideClimbableBlock(boolean p_70839_1_)
    {
        byte b0 = this.dataWatcher.getWatchableObjectByte(16);

        if (p_70839_1_)
        {
            b0 = (byte)(b0 | 1);
        }
        else
        {
            b0 &= -2;
        }

        this.dataWatcher.updateObject(16, Byte.valueOf(b0));
    }

    public IEntityLivingData func_180482_a(DifficultyInstance p_180482_1_, IEntityLivingData p_180482_2_)
    {
        Object p_180482_2_1 = super.func_180482_a(p_180482_1_, p_180482_2_);

        if (this.worldObj.rand.nextInt(100) == 0)
        {
            EntitySkeleton entityskeleton = new EntitySkeleton(this.worldObj);
            entityskeleton.setLocationAndAngles(this.posX, this.posY, this.posZ, this.rotationYaw, 0.0F);
            entityskeleton.func_180482_a(p_180482_1_, (IEntityLivingData)null);
            this.worldObj.spawnEntityInWorld(entityskeleton);
            entityskeleton.mountEntity(this);
        }

        if (p_180482_2_1 == null)
        {
            p_180482_2_1 = new EntitySpider.GroupData();

            if (this.worldObj.getDifficulty() == EnumDifficulty.HARD && this.worldObj.rand.nextFloat() < 0.1F * p_180482_1_.getClampedAdditionalDifficulty())
            {
                ((EntitySpider.GroupData)p_180482_2_1).func_111104_a(this.worldObj.rand);
            }
        }

        if (p_180482_2_1 instanceof EntitySpider.GroupData)
        {
            int i = ((EntitySpider.GroupData)p_180482_2_1).field_111105_a;

            if (i > 0 && Potion.potionTypes[i] != null)
            {
                this.addPotionEffect(new PotionEffect(i, Integer.MAX_VALUE));
            }
        }

        return (IEntityLivingData)p_180482_2_1;
    }

    public float getEyeHeight()
    {
        return 0.65F;
    }
    
    /**
     * Called when the entity is attacked.
     */
    public boolean attackEntityFrom(DamageSource source, float amount)
    {
        Entity entity0;

        entity0 = source.getEntity();

        if (entity0 != null && entity0.isInAlliedSystem && this.isInAlliedSystem && !this.isCorrupted && !entity0.isCorrupted)
        {
            return false;
        }
        else
        {
            return super.attackEntityFrom(source, amount);
        } 
    }
    public void onLivingUpdate()
    {
        if (this.ticksExisted % 10 == 0 && this.isOnLadder())
        {
        	this.playSound("mob.spider.step", 0.15F, 1.0F);
        }
        
        if (this.getAttackTarget() != null)
        {
            this.faceEntity(this.getAttackTarget(), 180, 40);
            
            if (this.rand.nextInt(120) == 0 && this.canEntityBeSeen(this.getAttackTarget()))
            {
                int i = MathHelper.floor_double(this.getAttackTarget().posX + this.rand.nextDouble() * 1.0D);
                int j = MathHelper.floor_double(this.getAttackTarget().posY + this.rand.nextDouble() * 1.0D);
                int k = MathHelper.floor_double(this.getAttackTarget().posZ + this.rand.nextDouble() * 1.0D);
                BlockPos blockpos = new BlockPos(i, j, k);
                Block block1 = this.worldObj.getBlockState(blockpos).getBlock();
                
                if (block1.getMaterial() == Material.air && this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"))
                {
                	this.worldObj.setBlockState(blockpos, Blocks.web.getDefaultState());
                }
                else
                {
                    this.getAttackTarget().addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 5 * 20, 2));
                }
            }
        }
    	
    	super.onLivingUpdate();
    }
    
    protected void updateAITasks()
    {
        super.updateAITasks();
        
        if (this.specialRechargeTimer >= 0)
        {
            if (this.getAttackTarget() != null && this.getAttackTarget().getDistanceSqToEntity(this) <= 24.0D * 24.0D && this.canEntityBeSeen(this.getAttackTarget()))
            {
                if (this.onGround && this.getAttackTarget().motionX <= 0.2 && this.getAttackTarget().motionZ <= 0.2)
                {
                    double d01 = getAttackTarget().posX - this.posX;
                    double d11 = getAttackTarget().posZ - this.posZ;
                    float f21 = MathHelper.sqrt_double(d01 * d01 + d11 * d11);
                	
                	double hor = f21 / 16 * 1.65;
                	double ver = 0.8D;
                	
                    this.motionX = d01 / (double)f21 * hor * hor + this.motionX * hor;
                    this.motionZ = d11 / (double)f21 * hor * hor + this.motionZ * hor;
                    this.motionY = ver;
                    this.specialRechargeTimer = -160;
                }
            }
        }
    }
    
    public boolean attackEntityAsMob(Entity p_70652_1_)
    {
        float f = (float)this.getEntityAttribute(SharedMonsterAttributes.attackDamage).getAttributeValue();
        this.worldObj.setEntityState(this, (byte)4);
        boolean flag = p_70652_1_.attackEntityFrom(DamageSource.causeMobDamage(this), (float)(f));
        
        if (flag && this.rand.nextInt(2) == 0)
        {
            int i = MathHelper.floor_double(this.getAttackTarget().posX + this.rand.nextDouble() * 1.0D);
            int j = MathHelper.floor_double(this.getAttackTarget().posY + this.rand.nextDouble() * 1.0D);
            int k = MathHelper.floor_double(this.getAttackTarget().posZ + this.rand.nextDouble() * 1.0D);
            BlockPos blockpos = new BlockPos(i, j, k);
            Block block1 = this.worldObj.getBlockState(blockpos).getBlock();
            
            if (block1.getMaterial() == Material.air && this.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing"))
            {
            	this.worldObj.setBlockState(blockpos, Blocks.web.getDefaultState());
            }
            else
            {
                this.getAttackTarget().addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 5 * 20, 2));
            }
        }

        return flag;
    }

    class AISpiderAttack extends EntityAIAttackOnCollide
    {
        private static final String __OBFID = "CL_00002197";

        public AISpiderAttack(Class p_i45819_2_)
        {
            super(EntitySpider.this, p_i45819_2_, 1.0D, true);
        }

        /**
         * Returns whether an in-progress EntityAIBase should continue executing
         */
        public boolean continueExecuting()
        {
            float f = this.attacker.getBrightness(1.0F);

            if (f >= 0.5F && this.attacker.getRNG().nextInt(100) == 0)
            {
                this.attacker.setAttackTarget((EntityLivingBase)null);
                return false;
            }
            else
            {
                return super.continueExecuting();
            }
        }

        protected double func_179512_a(EntityLivingBase p_179512_1_)
        {
            return (double)(4.0F + p_179512_1_.width);
        }
    }

    class AISpiderTarget extends EntityAINearestAttackableTarget
    {
        private static final String __OBFID = "CL_00002196";

        public AISpiderTarget(EntitySpider entitySpider, Class p_i45818_2_, int i, boolean b, boolean c, Predicate attackentityselector)
        {
            super(EntitySpider.this, p_i45818_2_, i, b, c, attackentityselector);
        }

        /**
         * Returns whether the EntityAIBase should begin execution.
         */
        public boolean shouldExecute()
        {
            float f = this.taskOwner.getBrightness(1.0F);
            return f >= 0.5F ? false : super.shouldExecute();
        }
    }

    public static class GroupData implements IEntityLivingData
        {
            public int field_111105_a;
            private static final String __OBFID = "CL_00001700";

            public void func_111104_a(Random p_111104_1_)
            {
                int i = p_111104_1_.nextInt(5);

                if (i <= 1)
                {
                    this.field_111105_a = Potion.moveSpeed.id;
                }
                else if (i <= 2)
                {
                    this.field_111105_a = Potion.damageBoost.id;
                }
                else if (i <= 3)
                {
                    this.field_111105_a = Potion.regeneration.id;
                }
                else if (i <= 4)
                {
                    this.field_111105_a = Potion.invisibility.id;
                }
            }
        }
}
