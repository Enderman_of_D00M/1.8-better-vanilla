package net.minecraft.entity.monster;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.IEntityLivingData;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.World;

public class EntityCaveSpider extends EntitySpider
{
    private static final String __OBFID = "CL_00001683";
    public int extraSpecialRechargeTimer;

    public EntityCaveSpider(World worldIn)
    {
        super(worldIn);
        this.setSize(0.9F, 0.5F);
    }

    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(16.0D);
    }
    
    public void onUpdate()
    {
        super.onUpdate();
        
    	if (this.extraSpecialRechargeTimer <= 0)
    	{
    		++this.extraSpecialRechargeTimer;
    	}
    	
    	if (this.extraSpecialRechargeTimer >= 0)
    	{
    		this.extraSpecialRechargeTimer = 0;
    	}
    }

    public boolean attackEntityAsMob(Entity p_70652_1_)
    {
        if (super.attackEntityAsMob(p_70652_1_))
        {
            if (p_70652_1_ instanceof EntityLivingBase)
            {
                byte b0 = 0;

                if (this.worldObj.getDifficulty() == EnumDifficulty.EASY)
                {
                    b0 = 2;
                }
                else if (this.worldObj.getDifficulty() == EnumDifficulty.NORMAL)
                {
                    b0 = 7;
                }
                else if (this.worldObj.getDifficulty() == EnumDifficulty.HARD)
                {
                    b0 = 15;
                }

                if (b0 > 0)
                {
                    ((EntityLivingBase)p_70652_1_).addPotionEffect(new PotionEffect(Potion.poison.id, b0 * 20, 0));
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    public IEntityLivingData func_180482_a(DifficultyInstance p_180482_1_, IEntityLivingData p_180482_2_)
    {
        return p_180482_2_;
    }

    public float getEyeHeight()
    {
        return 0.425F;
    }
    
    protected void updateAITasks()
    {
        super.updateAITasks();
        
        if (this.extraSpecialRechargeTimer >= 0)
        {
            if (this.getAttackTarget() != null && this.getAttackTarget().getDistanceSqToEntity(this) <= 12.0D * 12.0D && this.canEntityBeSeen(this.getAttackTarget()))
            {
                if (this.onGround)
                {
                    byte b0 = 0;
                    
                    PotionEffect b = null;

                    if (this.worldObj.getDifficulty() == EnumDifficulty.EASY)
                    {
                        b0 = 2;
                    }
                    else if (this.worldObj.getDifficulty() == EnumDifficulty.NORMAL)
                    {
                        b0 = 7;
                    }
                    else if (this.worldObj.getDifficulty() == EnumDifficulty.HARD)
                    {
                        b0 = 15;
                    }
                    
                    if (this.worldObj.getDifficulty() != EnumDifficulty.HARD)
                    {
                    	b = new PotionEffect(Potion.poison.id, b0 * 20, 1);
                    }
                    else
                    {
                    	b = new PotionEffect(Potion.wither.id, b0 * 20, 1);
                    }

                    if (b0 > 0)
                    {
                    	this.getAttackTarget().addPotionEffect(b);
                    }
                    
                    this.extraSpecialRechargeTimer = -300;
                }
            }
        }
    }
}
