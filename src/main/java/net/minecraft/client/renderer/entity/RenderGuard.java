package net.minecraft.client.renderer.entity;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelIronGolem;
import net.minecraft.client.model.ModelWither;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.layers.LayerHeldItem;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityGiantSkeleton;
import net.minecraft.entity.monster.EntityGuard;
import net.minecraft.util.ResourceLocation;

public class RenderGuard extends RenderBiped{

	public static final ResourceLocation steve_texture = new ResourceLocation("bettervanilla", "textures/monsters/golemsandguards/humanKnight.png");
	private static final String __OBFID = "CL_00000984";

    public RenderGuard(RenderManager p_i46168_1_)
    {
        super(p_i46168_1_, new ModelBiped(), 0.5F);
    }

    protected ResourceLocation getEntityTexture(EntityGuard entity)
    {
    	return steve_texture;
    }
    
    protected ResourceLocation getEntityTexture(EntityLiving p_110775_1_)
    {
        return this.getEntityTexture((EntityGuard)p_110775_1_);
    }

    protected ResourceLocation getEntityTexture(Entity entity)
    {
    	return this.getEntityTexture((EntityGuard)entity);
    }
}