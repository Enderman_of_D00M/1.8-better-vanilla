package net.minecraft.client.renderer.entity;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelBipedFemaleGuard;
import net.minecraft.client.model.ModelWither;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityGuard;
import net.minecraft.entity.monster.EntityGuardFemale;
import net.minecraft.util.ResourceLocation;

public class RenderGuardFemale extends RenderBiped{

	public static final ResourceLocation steve_texture = new ResourceLocation("bettervanilla", "textures/monsters/golemsandguards/humanKnightFemale.png");
	private static final String __OBFID = "CL_00000984";

    public RenderGuardFemale(RenderManager p_i46168_1_)
    {
        super(p_i46168_1_, new ModelBipedFemaleGuard(), 0.5F);
    }

    protected ResourceLocation getEntityTexture(EntityGuardFemale entity)
    {
    	return steve_texture;
    }
    
    protected ResourceLocation getEntityTexture(EntityLiving p_110775_1_)
    {
        return this.getEntityTexture((EntityGuardFemale)p_110775_1_);
    }

    protected ResourceLocation getEntityTexture(Entity entity)
    {
    	return this.getEntityTexture((EntityGuardFemale)entity);
    }
}