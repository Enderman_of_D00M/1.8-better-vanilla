package net.minecraft.client.renderer.entity;

import net.minecraft.client.model.ModelEnderGhast;
import net.minecraft.client.model.ModelGhast;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityEnderGhast;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderEnderGhast extends RenderLiving
{
    private static final ResourceLocation enderGhastTextures = new ResourceLocation("bettervanilla", "textures/monsters/normal/ender_ghast.png");
    private static final ResourceLocation enderGhastShootingTextures = new ResourceLocation("bettervanilla", "textures/monsters/normal/ender_ghast_shooting.png");
    private static final String __OBFID = "CL_00000997";

    public RenderEnderGhast(RenderManager p_i46174_1_, ModelEnderGhast modelGhast, float f, float g)
    {
        super(p_i46174_1_, modelGhast, f * g);
    }

    protected ResourceLocation func_180576_a(EntityEnderGhast p_180576_1_)
    {
        return p_180576_1_.func_110182_bF() ? enderGhastShootingTextures : enderGhastTextures;
    }

    /**
     * Allows the render to do any OpenGL state modifications necessary before the model is rendered. Args:
     * entityLiving, partialTickTime
     */
    protected void preRenderCallback(EntityEnderGhast p_77041_1_, float p_77041_2_)
    {
        float f1 = 1.0F;
        float f2 = (8.0F + f1) / 2.0F;
        float f3 = (8.0F + 1.0F / f1) / 2.0F;
        GlStateManager.scale(f3 * 3.0F, f2 * 3.0F, f3 * 3.0F);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
    }

    /**
     * Allows the render to do any OpenGL state modifications necessary before the model is rendered. Args:
     * entityLiving, partialTickTime
     */
    protected void preRenderCallback(EntityLivingBase p_77041_1_, float p_77041_2_)
    {
        this.preRenderCallback((EntityEnderGhast)p_77041_1_, p_77041_2_);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity entity)
    {
        return this.func_180576_a((EntityEnderGhast)entity);
    }
}