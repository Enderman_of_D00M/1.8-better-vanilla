package net.minecraft.client.renderer.entity;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelGiantSkeleton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.BossStatus;
import net.minecraft.entity.monster.EntityGiantSkeleton;
import net.minecraft.entity.monster.EntityGiantZombie;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class RenderGiantSkeleton extends RenderBiped
{
    private static final ResourceLocation skeletonTextures = new ResourceLocation("textures/entity/skeleton/skeleton.png");
    private static final ResourceLocation witherSkeletonTextures = new ResourceLocation("textures/entity/skeleton/wither_skeleton.png");
    private static final String __OBFID = "CL_00001023";

    public RenderGiantSkeleton(RenderManager p_i46143_1_, ModelGiantSkeleton modelGiantSkeleton, float f, float g)
    {
        super(p_i46143_1_, modelGiantSkeleton, f * g);
    }

    public void func_180591_a(EntityGiantSkeleton p_180591_1_, double p_180591_2_, double p_180591_4_, double p_180591_6_, float p_180591_8_, float p_180591_9_)
    {
        BossStatus.setBossStatus(p_180591_1_, true);
        super.doRender((EntityLiving)p_180591_1_, p_180591_2_, p_180591_4_, p_180591_6_, p_180591_8_, p_180591_9_);
    }
    
    protected void func_180592_a(EntityGiantSkeleton p_180592_1_, float p_180592_2_)
    {
        float f1 = 16.0F;
        int i = p_180592_1_.getInvulTime();

        if (i > 0)
        {
            f1 -= ((float)i - p_180592_2_) / 440.0F * 7.75F;
        }

        GlStateManager.scale(f1, f1, f1);
        
        if (p_180592_1_.getSkeletonType() == 1)
        {
            GL11.glScalef(1.2F, 1.2F, 1.2F);
        }
    }
    
    public void doRender(EntityLiving entity, double x, double y, double z, float p_76986_8_, float partialTicks)
    {
        this.func_180591_a((EntityGiantSkeleton)entity, x, y, z, p_76986_8_, partialTicks);
    }
    
    public void doRender(EntityLivingBase entity, double x, double y, double z, float p_76986_8_, float partialTicks)
    {
        this.func_180591_a((EntityGiantSkeleton)entity, x, y, z, p_76986_8_, partialTicks);
    }
    
    public void doRender(Entity entity, double x, double y, double z, float p_76986_8_, float partialTicks)
    {
        this.func_180591_a((EntityGiantSkeleton)entity, x, y, z, p_76986_8_, partialTicks);
    }

    public void func_82422_c()
    {
        GlStateManager.translate(0.0F, 0.15F, 0.0F);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(EntityGiantSkeleton p_110775_1_)
    {
        return p_110775_1_.getSkeletonType() == 1 ? witherSkeletonTextures : skeletonTextures;
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(EntityLiving p_110775_1_)
    {
        return this.getEntityTexture((EntityGiantSkeleton)p_110775_1_);
    }

    /**
     * Allows the render to do any OpenGL state modifications necessary before the model is rendered. Args:
     * entityLiving, partialTickTime
     */
    protected void preRenderCallback(EntityLivingBase p_77041_1_, float p_77041_2_)
    {
        this.func_180592_a((EntityGiantSkeleton)p_77041_1_, p_77041_2_);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity p_110775_1_)
    {
        return this.getEntityTexture((EntityGiantSkeleton)p_110775_1_);
    }
    
    protected void func_180588_a(EntityGiantSkeleton p_180588_1_, float p_180588_2_, float p_180588_3_, float p_180588_4_)
    {
        super.rotateCorpse(p_180588_1_, p_180588_2_, p_180588_3_, p_180588_4_);

        if ((double)p_180588_1_.limbSwingAmount >= 0.01D)
        {
            float f3 = 13.0F;
            float f4 = p_180588_1_.limbSwing - p_180588_1_.limbSwingAmount * (1.0F - p_180588_4_) + 6.0F;
            float f5 = (Math.abs(f4 % f3 - f3 * 0.5F) - f3 * 0.25F) / (f3 * 0.25F);
            GlStateManager.rotate(8.0F * f5, 0.0F, 0.0F, 1.0F);
        }
    }

    protected void rotateCorpse(EntityLivingBase p_77043_1_, float p_77043_2_, float p_77043_3_, float p_77043_4_)
    {
        this.func_180588_a((EntityGiantSkeleton)p_77043_1_, p_77043_2_, p_77043_3_, p_77043_4_);
    }
}