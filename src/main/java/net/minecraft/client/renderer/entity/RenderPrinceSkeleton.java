package net.minecraft.client.renderer.entity;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelPrinceSkeleton;
import net.minecraft.client.model.ModelSkeleton;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.BossStatus;
import net.minecraft.entity.boss.EntityKingZombie;
import net.minecraft.entity.boss.EntityPrinceSkeleton;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class RenderPrinceSkeleton extends RenderBiped
{
    private static final ResourceLocation skeletonTextures = new ResourceLocation("bettervanilla", "textures/monsters/bosses/skeletonPrince.png");
    private static final String __OBFID = "CL_00001023";

    public RenderPrinceSkeleton(RenderManager p_i46143_1_)
    {
        super(p_i46143_1_, new ModelPrinceSkeleton(), 0.5F);
    }
    
    public void func_180579_a(EntityPrinceSkeleton p_180579_1_, double p_180579_2_, double p_180579_4_, double p_180579_6_, float p_180579_8_, float p_180579_9_)
    {
        BossStatus.setBossStatus(p_180579_1_, true);
        super.doRender(p_180579_1_, p_180579_2_, p_180579_4_, p_180579_6_, p_180579_8_, p_180579_9_);
    }
    
    public void doRender(EntityLivingBase entity, double x, double y, double z, float p_76986_8_, float partialTicks)
    {
        this.func_180579_a((EntityPrinceSkeleton)entity, x, y, z, p_76986_8_, partialTicks);
    }
    
    public void doRender(EntityLiving entity, double x, double y, double z, float p_76986_8_, float partialTicks)
    {
        this.func_180579_a((EntityPrinceSkeleton)entity, x, y, z, p_76986_8_, partialTicks);
    }
    
    public void doRender(Entity entity, double x, double y, double z, float p_76986_8_, float partialTicks)
    {
        this.func_180579_a((EntityPrinceSkeleton)entity, x, y, z, p_76986_8_, partialTicks);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(EntityPrinceSkeleton p_110775_1_)
    {
        return skeletonTextures;
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(EntityLiving p_110775_1_)
    {
        return this.getEntityTexture((EntityPrinceSkeleton)p_110775_1_);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity p_110775_1_)
    {
        return this.getEntityTexture((EntityPrinceSkeleton)p_110775_1_);
    }
}