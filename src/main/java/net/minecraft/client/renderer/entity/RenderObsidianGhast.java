package net.minecraft.client.renderer.entity;

import net.minecraft.client.model.ModelEnderGhast;
import net.minecraft.client.model.ModelGhast;
import net.minecraft.client.model.ModelObsidianGhast;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.boss.BossStatus;
import net.minecraft.entity.boss.EntityObsidianGhast;
import net.minecraft.entity.boss.EntityPrinceSkeleton;
import net.minecraft.entity.boss.EntityWitherGood;
import net.minecraft.entity.monster.EntityEnderGhast;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class RenderObsidianGhast extends RenderLiving
{
    private static final ResourceLocation obsidianGhastTextures = new ResourceLocation("bettervanilla", "textures/monsters/bosses/obsidian_ghast.png");
    private static final ResourceLocation obsidianGhastShootingTextures = new ResourceLocation("bettervanilla", "textures/monsters/bosses/obsidian_ghast_shooting.png");
    private static final String __OBFID = "CL_00000997";

    public RenderObsidianGhast(RenderManager p_i46174_1_, ModelObsidianGhast modelGhast, float f, float g)
    {
        super(p_i46174_1_, modelGhast, f * g);
    }
    
    public void func_180579_a(EntityObsidianGhast p_180579_1_, double p_180579_2_, double p_180579_4_, double p_180579_6_, float p_180579_8_, float p_180579_9_)
    {
        BossStatus.setBossStatus(p_180579_1_, true);
        super.doRender(p_180579_1_, p_180579_2_, p_180579_4_, p_180579_6_, p_180579_8_, p_180579_9_);
    }
    
    public void doRender(EntityLivingBase entity, double x, double y, double z, float p_76986_8_, float partialTicks)
    {
        this.func_180579_a((EntityObsidianGhast)entity, x, y, z, p_76986_8_, partialTicks);
    }
    
    public void doRender(EntityLiving entity, double x, double y, double z, float p_76986_8_, float partialTicks)
    {
        this.func_180579_a((EntityObsidianGhast)entity, x, y, z, p_76986_8_, partialTicks);
    }
    
    public void doRender(Entity entity, double x, double y, double z, float p_76986_8_, float partialTicks)
    {
        this.func_180579_a((EntityObsidianGhast)entity, x, y, z, p_76986_8_, partialTicks);
    }

    protected ResourceLocation func_180576_a(EntityObsidianGhast p_180576_1_)
    {
        return p_180576_1_.func_110182_bF() ? obsidianGhastShootingTextures : obsidianGhastTextures;
    }

    /**
     * Allows the render to do any OpenGL state modifications necessary before the model is rendered. Args:
     * entityLiving, partialTickTime
     */
    protected void preRenderCallback(EntityObsidianGhast p_77041_1_, float p_77041_2_)
    {
        float f1 = 1.0F;
        float f2 = (8.0F + f1) / 2.0F;
        float f3 = (8.0F + 1.0F / f1) / 2.0F;
        GlStateManager.scale(f3 * 2.0F, f2 * 2.0F, f3 * 2.0F);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
    }

    /**
     * Allows the render to do any OpenGL state modifications necessary before the model is rendered. Args:
     * entityLiving, partialTickTime
     */
    protected void preRenderCallback(EntityLivingBase p_77041_1_, float p_77041_2_)
    {
        this.preRenderCallback((EntityObsidianGhast)p_77041_1_, p_77041_2_);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    protected ResourceLocation getEntityTexture(Entity entity)
    {
        return this.func_180576_a((EntityObsidianGhast)entity);
    }
}