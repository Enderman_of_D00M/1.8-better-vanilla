package net.minecraft.client.model;

import java.util.Random;

import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.EntityEnderGhast;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.util.MathHelper;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModelEnderGhast extends ModelBase
{
    ModelRenderer body;
    ModelRenderer[] tentacles = new ModelRenderer[9];
    ModelRenderer extraTentacle;
    private static final String __OBFID = "CL_00000839";

    public ModelEnderGhast()
    {
        byte b0 = -16;
        this.body = new ModelRenderer(this, 0, 0);
        this.body.addBox(-8.0F, -8.0F, -8.0F, 16, 16, 16);
        this.body.rotationPointY += (float)(24 + b0);
        Random random = new Random(1660L);

        for (int i = 0; i < this.tentacles.length; ++i)
        {
            this.tentacles[i] = new ModelRenderer(this, 0, 0);
            float f = (((float)(i % 3) - (float)(i / 3 % 2) * 0.5F + 0.25F) / 2.0F * 2.0F - 1.0F) * 5.0F;
            float f1 = ((float)(i / 3) / 2.0F * 2.0F - 1.0F) * 5.0F;
            int j = random.nextInt(7) + 8;
            this.tentacles[i].addBox(-1.0F, 0.0F, -1.0F, 2, j, 2);
            this.tentacles[i].rotationPointX = f;
            this.tentacles[i].rotationPointZ = f1;
            this.tentacles[i].rotationPointY = (float)(31 + b0);
        }
        
        this.extraTentacle = new ModelRenderer(this, 0, 0);
        this.extraTentacle.addBox(-1.0F, 0.0F, -1.0F, 2, 16, 2);
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    public void setRotationAngles(float p_78087_1_, float p_78087_2_, float p_78087_3_, float p_78087_4_, float p_78087_5_, float p_78087_6_, Entity p_78087_7_)
    {
        for (int i = 0; i < this.tentacles.length; ++i)
        {
            this.tentacles[i].rotateAngleX = 0.25F * MathHelper.sin(p_78087_3_ * 0.125F + (float)i) + 0.6F;
        }
        
        float f6 = MathHelper.cos(p_78087_3_ * 0.1F);
        this.extraTentacle.rotateAngleX = (0.1F + 0.05F * f6) * (float)Math.PI;
        
        this.extraTentacle.rotationPointX = 1.0F;
        this.extraTentacle.rotationPointY = 23.0F;
        this.extraTentacle.rotationPointZ = 2.0F;
    }

    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity p_78088_1_, float p_78088_2_, float p_78088_3_, float p_78088_4_, float p_78088_5_, float p_78088_6_, float p_78088_7_)
    {
        this.setRotationAngles(p_78088_2_, p_78088_3_, p_78088_4_, p_78088_5_, p_78088_6_, p_78088_7_, p_78088_1_);
        GlStateManager.pushMatrix();
        GlStateManager.translate(0.0F, 0.5F, 0.0F);
        this.body.render(p_78088_7_);
        ModelRenderer[] amodelrenderer = this.tentacles;
        int i = amodelrenderer.length;

        for (int j = 0; j < i; ++j)
        {
            ModelRenderer modelrenderer = amodelrenderer[j];
            modelrenderer.render(p_78088_7_);
        }

        GlStateManager.popMatrix();
        
        if (((EntityEnderGhast)p_78088_1_).getGender() == 1)
        {
            this.extraTentacle.render(p_78088_7_);
        }
    }
}