package com.mod.BetterVanilla;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class CraftingandSmeltingHandler {
	
    private static ItemStack itemstack;

	
	public static void BetterVanilla()
	{
		addCraftingRecipes();
		addSmeltingRecipes();
	}
	
public static void addCraftingRecipes()
{
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 1, 50), new Object[] {Items.egg, Items.gunpowder});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 1, 51), new Object[] {Items.egg, Items.arrow, Items.bone});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 2, 52), new Object[] {Items.egg, Items.string});
	GameRegistry.addShapedRecipe(new ItemStack(Items.spawn_egg, 1, 53), "RRR", "RER", "RRR", 'R', Items.rotten_flesh, 'E', Items.egg);
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 1, 54), new Object[] {Items.egg, Items.rotten_flesh});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 1, 55), new Object[] {Items.egg, Items.slime_ball});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 1, 56), new Object[] {Items.egg, Items.gunpowder, Items.ghast_tear});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 1, 57), new Object[] {Items.egg, Items.rotten_flesh, Items.gold_nugget});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 1, 58), new Object[] {Items.egg, Items.ender_pearl});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 2, 59), new Object[] {Items.egg, Items.string, Items.spider_eye});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 4, 60), new Object[] {Items.egg, Blocks.stone});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 4, 60), new Object[] {Items.egg, Blocks.stonebrick});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 4, 60), new Object[] {Items.egg, Blocks.cobblestone});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 1, 61), new Object[] {Items.egg, Items.blaze_rod});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 2, 62), new Object[] {Items.egg, Items.magma_cream});
	GameRegistry.addShapedRecipe(new ItemStack(Items.spawn_egg, 1, 63), "OOO", "EDE", "OOO", 'O', Blocks.obsidian, 'E', Items.ender_eye, 'D', Blocks.dragon_egg);
	GameRegistry.addShapedRecipe(new ItemStack(Items.spawn_egg, 1, 64), "SSS", "UUU", "EUE", 'S', new ItemStack(Items.skull, 1, 1), 'U', Blocks.soul_sand, 'E', Items.egg);
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 4, 65), new Object[] {Items.egg});
	GameRegistry.addShapelessRecipe(new ItemStack(Items.spawn_egg, 4, 66), new Object[] {Items.egg, Items.glass_bottle});
}
public static void addSmeltingRecipes()
{
	GameRegistry.addSmelting(new ItemStack(Items.rotten_flesh, 1), new ItemStack(Items.leather, 1), 10.0F);
}
}
